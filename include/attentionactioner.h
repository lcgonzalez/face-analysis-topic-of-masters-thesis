#ifndef ACTIONER_H
#define ACTIONER_H

#include "actionerinterface.h"
#include "imagedatabase.h"
#include "imagelogger.h"

#include <boost/circular_buffer.hpp>
#include <opencv2/core/core.hpp>
#include <fstream>

#include <vector>

namespace lcg_face_attention {

class AttentionActioner : public ActionerInterface
{
    boost::circular_buffer<bool> cb;
    int t;
    bool attention_;
    bool bChangeImage_;
    boost::mutex mtx_;
    int attLength_;
    ImageDatabase* pImageDatabase_;
    cv::Mat image1;
    cv::Mat decoyImg_;
    int iImageTime_;
    bool bDrawingAttention_;
    std::ofstream file_;
    long long int dataEntry;
    long long int maxDataEntries;
    ImageLogger imageLogger;
    std::string resultsFilename;
public:
    AttentionActioner();
    virtual ~AttentionActioner();
    virtual void updateAction(ActionEvent event);
    virtual void action();
    void setImageDatabase(ImageDatabase *pNewImageDatabase);
    void setImageTime(int iNewImageTime_);
    /**
     * @brief logAttentionRatio Logs the ratio between the calculated user attention
     * (from the face analysis algorithm) versus the actual suposed focus of attention
     * represented by the real and decoy image being displayed.
     */
    void logAttentionRatio();
    void setResultsFilename(std::string newResultsFilename);
    void setMaxDataEntries(long long int newMaxDataEntries);
};

} // namespace lcg_face_attention

#endif // ACTIONER_H
