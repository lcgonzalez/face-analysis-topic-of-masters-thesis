#ifndef IMAGELOGGER_H
#define IMAGELOGGER_H

#include <opencv2/core/core.hpp>
#include <string>

namespace lcg_face_attention {

class ImageLogger
{
private:
    short int textRow;

public:
    ImageLogger();
    void drawTitle(std::string text, cv::Mat &img);
    void drawText(std::string text, cv::Mat &img);
    void resetTextRowToBegining();
};

} // namespace lcg_face_attention

#endif // IMAGELOGGER_H
