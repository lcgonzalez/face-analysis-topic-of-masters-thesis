#ifndef USEFULDEFINITIONS_H
#define USEFULDEFINITIONS_H

namespace lcg_face_attention {

// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&);               \
    void operator=(const TypeName&)
    // Define dummy declarations for the copy constructor and assignment operator
    // But don't define them, so that any attempt to use them results in a link error

} // namespace lcg_face_attention

#endif // USEFULDEFINITIONS_H

