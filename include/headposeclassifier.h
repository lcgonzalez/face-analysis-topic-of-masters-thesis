// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#ifndef HEADPOSECLASSIFIER_H
#define HEADPOSECLASSIFIER_H

#include <queue>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include "enhancedcascadeclassifier.h"
#include "imagelogger.h"
#include "recttransformations.h"

/**
 * @file headposeclassifier.h
 * @author Luis Carlos Gonzalez Garcia <lc.gonzalez23@gmail.com>
 * @version 1.0
 * @date Feb/28/2012
 *
 * @section LICENSE
 *
 * This file ...
 *
 * @section DESCRIPTION
 *
 * Contiene las declaraciones necesarias para la clase RR::Env.
 * Descripción extensa, de ser necesaria ...
 *
 */

namespace lcg_face_attention {

/**
  * TODO Add class documentation
  * @see EnhancedCascadeClassifier
  */
class HeadPoseClassifier
{
private:
    std::queue<EnhancedCascadeClassifier> headPosesClassifiers_;
    short int scale_;
    short int numberDetectionTime;
    cv::Mat rotateImage(const cv::Mat& rSource, double angle) const;
    void applyClassifierTransformations(cv::Mat& rImage,
                                        const EnhancedCascadeClassifier &rFaceClassifier) const;

    ImageLogger *imageLogger_;
    RectTransformations rectTransformations_;
public:
    HeadPoseClassifier();

    /**
     * @brief  Adds an EhnacedCascadeClassifier to the list of detectors.
     * Each detector represent a specific head pose.
     * @param  headPoseCascace An EhnacedCascadeClassifier representing a head pose.
     * @see detect()
     */
    inline void addClassifier(EnhancedCascadeClassifier headPoseCascace);
    std::vector<cv::Rect> detect(cv::Mat& rOriginalImage);
    inline void setScale(int newScale);
    inline short int getScale() const;
    inline void setImageLogger(ImageLogger *pewImageLogger);
    inline ImageLogger* getImageLogger();
};

//---------> Setters and getters
inline void HeadPoseClassifier::addClassifier(EnhancedCascadeClassifier headPoseCascace)
{
    headPosesClassifiers_.push(headPoseCascace);
}

inline void HeadPoseClassifier::setScale(int newScale)
{
    scale_ = newScale;
}

inline short HeadPoseClassifier::getScale() const
{
    return scale_;
}

inline void HeadPoseClassifier::setImageLogger(ImageLogger *pNewImageLogger)
{
    imageLogger_ = pNewImageLogger;
}

inline ImageLogger *HeadPoseClassifier::getImageLogger()
{
    return imageLogger_;
}

} // namespace lcg_face_attention


#endif // HEADPOSECLASSIFIER_H
