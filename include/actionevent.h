#ifndef ACTIONEVENT_H
#define ACTIONEVENT_H

namespace lcg_face_attention {

class ActionEvent
{
    bool running;
public:
    ActionEvent();
    void setRunning(bool newRunning);
    bool isRunning();
};

} // namespace lcg_face_attention

#endif // ACTIONEVENT_H
