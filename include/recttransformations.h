#ifndef RECTTRANSFORMATIONS_H
#define RECTTRANSFORMATIONS_H

#include <opencv2/core/core.hpp>

namespace lcg_face_attention {

class RectTransformations
{
public:
    RectTransformations();
    void scale(cv::Rect& rRectangleToScale, const float kScale) const; //scale opencv cv::Rect objects
    cv::Rect relativeScale( const cv::Rect rRectangleToScale, cv::Rect_<float> relativeRectangle ) const;
    void absoluteScale(cv::Rect& rRectangleToScale, const short int kScale);
    void absoluteScale(std::vector<cv::Rect> &rRectanglesToScaleList, const short int kScale);
};

} // namespace lcg_face_attention

#endif // RECTTRANSFORMATIONS_H
