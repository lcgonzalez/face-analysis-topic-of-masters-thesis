// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#ifndef ENHANCEDCASCADECLASSIFIER_H
#define ENHANCEDCASCADECLASSIFIER_H

#include <opencv2/objdetect/objdetect.hpp>
#include <string>

#include "recttransformations.h"

namespace lcg_face_attention {

/**
 * TODO Add class documentation
 */
class EnhancedCascadeClassifier : public cv::CascadeClassifier
{
private:
    std::string classifierName_;
    bool rotateImage_;
    float imageRotationAngle_;
    cv::Rect_<float> relativeArea_;
    double scaleFactor_;
    int minNeighbors_;
    int flags_;
    cv::Size minSize_;
    cv::Size maxSize_;
    RectTransformations rectTransformations_;

public:
    EnhancedCascadeClassifier();
    inline void setName(const std::string& rNewCascadeName);
    inline void setMirror(bool mirror);
    inline void setRotationAngle(float rNewRotationAngle);
    inline std::string getName();
    inline bool isMirror() const;
    inline float getRotation() const;
    inline void setSearchArea(cv::Rect_<float> relativeArea_);
    cv::Rect resizeSearchArea(const cv::Rect searchArea) const;

    /**
     * @brief setScaleFactor Function used to specify the scaleFactor parameter
     * of the CascadeClassifier::detectMultiScale method.
     * @param newScaleFactor Parameter specifying how much the image size is
     * reduced at each image scale.
     */
    inline void setScaleFactor(const double& rNewScaleFactor);
    inline double getScaleFactor();

    /**
     * @brief setMinNeighbors Function used to specify the minNeighbors
     * parameter of the CascadeClassifier::detectMultiScale method.
     * @param newMinNeighbors Parameter specifying how many neighbors each
     * candidate rectangle should have to retain it.
     */
    inline void setMinNeighbors(const int& rNewMinNeighbors);
    inline int getMinNeighbors();

    /**
     * @brief setFlags Function used to specify the flags parameter of
     * the CascadeClassifier::detectMultiScale method.
     * @param newFlags Parameter with the same meaning for an old cascade as
     * in the function cvHaarDetectObjects. It is not used for a new cascade.
     */
    inline void setFlags(const int& rNewFlags);
    inline int getFlags();

    /**
     * @brief setMinSize Function used to specify the minSize parameter of
     * the CascadeClassifier::detectMultiScale method.
     * @param newMinSize Minimum possible object size. Objects smaller than
     * that are ignored.
     */
    inline void setMinSize(const cv::Size& rNewMinSize);
    inline cv::Size getMinSize();

    /**
     * @brief setMaxSize Function used to specify the maxSize parameter of
     * the CascadeClassifier::detectMultiScale method.
     * @param newMaxSize Maximum possible object size. Objects larger than
     * that are ignored.
     */
    inline void setMaxSize(const cv::Size& rNewMaxSize);
    inline cv::Size getMaxSize();
};

//-------> Inline setters and getters
inline void EnhancedCascadeClassifier::setName(const std::string &rNewCascadeName)
{
    classifierName_ = rNewCascadeName;
}

inline void EnhancedCascadeClassifier::setMirror(bool mirror)
{
    rotateImage_ = mirror;
}

inline void EnhancedCascadeClassifier::setRotationAngle(float rNewRotationAngle)
{
    imageRotationAngle_ = rNewRotationAngle;
}

inline std::string EnhancedCascadeClassifier::getName()
{
    return classifierName_;
}

inline bool EnhancedCascadeClassifier::isMirror() const
{
    return rotateImage_;
}

inline float EnhancedCascadeClassifier::getRotation() const
{
    return imageRotationAngle_;
}

inline void EnhancedCascadeClassifier::setSearchArea(cv::Rect_<float> relativeArea)
{
    this->relativeArea_ = relativeArea;
}

inline void EnhancedCascadeClassifier::setScaleFactor(const double &rNewScaleFactor)
{
    scaleFactor_ = rNewScaleFactor;
}

inline double EnhancedCascadeClassifier::getScaleFactor()
{
    return scaleFactor_;
}

inline void EnhancedCascadeClassifier::setMinNeighbors(const int &rNewMinNeighbors)
{
    minNeighbors_ = rNewMinNeighbors;
}

inline int EnhancedCascadeClassifier::getMinNeighbors()
{
    return minNeighbors_;
}

inline void EnhancedCascadeClassifier::setFlags(const int &rNewFlags)
{
    flags_ = rNewFlags;
}

inline int EnhancedCascadeClassifier::getFlags()
{
    return flags_;
}

inline void EnhancedCascadeClassifier::setMinSize(const cv::Size &rNewMinSize)
{
    minSize_ = rNewMinSize;
}

inline cv::Size EnhancedCascadeClassifier::getMinSize()
{
    return minSize_;
}

inline void EnhancedCascadeClassifier::setMaxSize(const cv::Size &rNewMaxSize)
{
    maxSize_ = rNewMaxSize;
}

inline cv::Size EnhancedCascadeClassifier::getMaxSize()
{
    return maxSize_;
}

} // namespace lcg_face_attention

#endif // ENHANCEDCASCADECLASSIFIER_H
