#ifndef IMAGEDATABASE_H
#define IMAGEDATABASE_H

#include<string>
#include<vector>
namespace lcg_face_attention {

class ImageDatabase
{
    std::vector<std::string> ext;
    std::vector<std::string> files;
public:
    ImageDatabase();
    /**
     * @brief loadFiles
     * Load all image files (recursively) from the directory defined by path,
     * and with the extensions defined by ext.
     * @param path
     */
    void loadFiles(std::string path);

    /**
     * @brief getImage
     * Get a random image path each time it's called.
     * @return String resembling a path to an image (randomly) selected.
     */
    std::string getImage();
};

} // namespace lcg_face_attention

#endif // IMAGEDATABASE_H
