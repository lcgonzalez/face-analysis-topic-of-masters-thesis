// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#ifndef EYEATTENTION_H
#define EYEATTENTION_H

#include <vector>
#include <boost/signals2/signal.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include "attentionevent.h"
#include "attentioninterface.h"
#include "headposeclassifier.h"
#include "recttransformations.h"
#include "usefulDefinitions.h"

namespace lcg_face_attention {

class EyeAttentionDetector : public AttentionInterface
{
public:
    EyeAttentionDetector();
    ~EyeAttentionDetector();

    void pushClassifier(const EnhancedCascadeClassifier &rLeftEyeCascade);

    /**
     * Detects if a face is giving attention to the camera
     *
     * The function automatically sends a signal through the slot connected
     * using the EyeAttention::connect method, whenever the attention of the
     * face is calculated.
     * @param faces Vector of rectangles that define the bounding box of faces
     * contained inside an image.
     * @return Boolean value describing whether or not the face is attentive
     * @see EyeAttention::connect
     */
    bool detect(cv::Mat &rFaceImage, const std::vector<cv::Rect> kFacesList);
    void setImageLogger(ImageLogger *pNewImageLogger);
    ImageLogger *getImageLogger();

    virtual AttentionConnection connect(const AttentionSignal::slot_function_type kSubscriber);
    virtual void disconnect(const AttentionConnection kSubscriber);

private:
    void generateColors();
    std::vector<cv::Rect> detectObject(cv::Mat &rImage,
                                       EnhancedCascadeClassifier &rObjectCascade,
                                       const cv::Rect &rTotalSearchingArea);
    void detectGaze(cv::Mat &rOriginalImage, const cv::Rect &rEyeArea);
    cv::Mat preprocessEyeImage(const cv::Mat &rEyeImage) const;

    std::vector<EnhancedCascadeClassifier> classifierList_;
    ImageLogger *pImageLogger_; //Usesful for logging to image

    bool attention_;
    AttentionEvent* pAttentionEvent_;
    cv::Scalar* pColors_;
    RectTransformations rectTransformations_;

    AttentionSignal AttentionEvent_signal_;

    DISALLOW_COPY_AND_ASSIGN(EyeAttentionDetector);
};

} // namespace lcg_face_attention

#endif // EYEATTENTION_H
