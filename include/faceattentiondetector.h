#ifndef FACEATTENTIONDETECTOR_H
#define FACEATTENTIONDETECTOR_H

#include "attentioninterface.h"
#include "eyeattentiondetector.h"
#include "headposeclassifier.h"
#include "imagelogger.h"

namespace lcg_face_attention {

class FaceAttentionDetector : public AttentionInterface
{
public:
    FaceAttentionDetector();
    ~FaceAttentionDetector();

    virtual bool detect(cv::Mat &rImage, const std::vector<cv::Rect> kSearchingAreas);
    virtual AttentionConnection connect(AttentionSignal::slot_function_type subscriber);
    virtual void disconnect(AttentionConnection subscriber);

    void setEyeAttentionDetector(EyeAttentionDetector* pNewEyeAttentionDetector);
    void setHeadPoseClassifier(HeadPoseClassifier* pNewHeadPoseClassifier);
    void setImageLogger(ImageLogger *pNewImageLogger);

private:
    void updateImageLogger();

    AttentionSignal AttentionEvent_signal_;
    EyeAttentionDetector *pEyeAttentionDetector_;
    HeadPoseClassifier *pHeadPoseClassifier_;
    ImageLogger *pImageLogger_;

    AttentionInterface::AttentionConnection  eyeAttentionDetector_connection;
};

} // namespace lcg_face_attention

#endif // FACEATTENTIONDETECTOR_H
