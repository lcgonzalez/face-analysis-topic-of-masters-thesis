// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#ifndef RUNSETTINGSREADER_H
#define RUNSETTINGSREADER_H

#include <boost/property_tree/ptree.hpp>
#include <map>
#include <string>

namespace lcg_face_attention {

class Settings
{
private:
    /**
     * @brief populateAllSubnodes Recursive function used to populate the
     * xmlNodeValues_ with all the xml nodes (elements) names and values
     * of the configuration file.
     * @param xmlTree Tree of xml file representing all the nodes within it
     * @param xmlTreeNode Node nmae under \a xmlTree used used to populate
     * xmlNodeValues_
     * @param nodeQualifiedName Full qualified name of the current node. Useful
     * to use the qualified name as the key to store the node value in
     * xmlNodeValues_
     */
    void populateAllSubnodes(boost::property_tree::ptree xmlTree,
                             std::string xmlTreeNode = "",
                             std::string nodeQualifiedName = "");

    std::map<std::string, std::string> xmlNodeValues_;

    /**
     * @brief configFilename_ Path of the configuration file.
     */
    std::string configFilename_;

public:
    /**
     * @brief Settings Default constructor
     */
    Settings();

    /**
     * @brief log Log on terminal the settings loaded from the configuration
     * file.
     */
    void log();

    /**
     * @brief getParameter Query a parameter value defined on the xml
     * configuration file.
     * @param parameterName Defines the parameter from which the value would
     * be queried. The parameter name is defined by the full qualified name of
     * the xml node (element) desired, separating each node path by a dot ".".
     * Examples of such qualified names would be:
     * run.classifiers.frontal_face
     * xmlElement1.xmlElement2.xmlElement3
     * @example getParameter("run.classifiers.frontal_face")
     * @return The value of the parameter defined by parameterName.
     */
    inline std::string getParameter(std::string parameterName);

    /**
     * @brief readSettings Read the setting from the confifuration file.
     * @param filename Path and name of the configuration file.
     */
    void readSettings(const std::string &filename);
};

inline std::string Settings::getParameter(std::string parameterName)
{
    return xmlNodeValues_[parameterName];
}

} // Namespace lcg_face_attention

#endif // RUNSETTINGSREADER_H
