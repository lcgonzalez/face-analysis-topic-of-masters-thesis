#ifndef ACTIONERINTERFACE_H
#define ACTIONERINTERFACE_H

#include "actionevent.h"

#include <boost/thread.hpp>

namespace lcg_face_attention {

class ActionerInterface
{
protected:
    boost::thread m_Thread;
public:
    ActionerInterface();
    virtual void updateAction(ActionEvent event) =0;
    /**
     * @brief action should contain the actual actions to do according to the
     * ActionEvent received on updateAction method.
     */
    virtual void action() =0;
    virtual void join();
    virtual ~ActionerInterface();
};

} // namespace lcg_face_attention

#endif // ACTIONERINTERFACE_H
