## Copyright (c) 2013, Luis Carlos González García, all rights reserved.

project(tests)
cmake_minimum_required(VERSION 2.8)
aux_source_directory(. SRC_LIST_TESTS)
aux_source_directory(../src SRC_LIST)

include_directories(../include)

## Set the build type if it isn't already
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

# Enable ExternalProject CMake module
include(ExternalProject)

# Set default ExternalProject root directory
set_directory_properties(PROPERTIES EP_PREFIX ${CMAKE_SOURCE_DIR}/gmock)

# Add gmock
set(gmock_source_rep http://googlemock.googlecode.com/svn/trunk/)
ExternalProject_Add(
    googlemock
    SVN_REPOSITORY ${gmock_source_rep}
    TIMEOUT 30
    # Force separate output paths for debug and release builds to allow easy
    # identification of correct lib in subsequent TARGET_LINK_LIBRARIES commands
    CMAKE_ARGS -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG:PATH=DebugLibs
               -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE:PATH=ReleaseLibs
               -Dgtest_force_shared_crt=ON
    # Disable install step
    INSTALL_COMMAND ""
    UPDATE_COMMAND svn checkout ${gmock_source_rep}
    # Wrap download, configure and build steps in a script to log output
    LOG_DOWNLOAD ON
    LOG_CONFIGURE ON
    LOG_BUILD ON

)

# Specify include directory for googlemock and googletest
ExternalProject_Get_Property(googlemock source_dir)
include_directories(${source_dir}/include)
include_directories(${source_dir}/gtest/include)

ExternalProject_Get_Property(googlemock binary_dir)
# Link the library
LINK_DIRECTORIES(${binary_dir})

FIND_PACKAGE(Threads REQUIRED) #for pthread (required for gmock)
find_package(OpenCV REQUIRED)
find_package(Boost 1.46 REQUIRED COMPONENTS system filesystem thread)

add_executable(${PROJECT_NAME} ${SRC_LIST_TESTS} ${SRC_LIST})
TARGET_LINK_LIBRARIES( ${PROJECT_NAME} gmock)
TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${CMAKE_THREAD_LIBS_INIT}) #pthread
target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS})
target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES})

# Create dependency of MainTest on googlemock
add_dependencies(${PROJECT_NAME} googlemock)



