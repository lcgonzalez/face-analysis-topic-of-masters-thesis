// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#include "recttransformations.h"

#include <gmock/gmock.h>
#include <opencv2/core/core.hpp>

class RectTransformationsScale : public ::testing::Test
{
public:

    lcg_face_attention::RectTransformations rectTransformations;
    cv::Rect rectTesting, rectScaled;
    double scaleFactor;

    void SetUp(){
        rectTesting.x = 100;
        rectTesting.width = 100;
        rectTesting.y = 100;
        rectTesting.height = 100;
    }
};

//Test scaling the rectangle to the same size (scaleFactor = 1)
TEST_F(RectTransformationsScale, Scale_Equal) {
    //Create a rectangle with the same size of rectTesting
    rectScaled.x = 100;
    rectScaled.width = 100;
    rectScaled.y = 100;
    rectScaled.height = 100;

    scaleFactor = 1;
    rectTransformations.scale(rectTesting, scaleFactor);
    ASSERT_THAT(rectTesting, testing::Eq(rectScaled));
}

//Test scaling the rectangle to a bigger size (scaleFactor > 1)
TEST_F(RectTransformationsScale, Scale_GraterThanOne) {
    //Create a rectangle with double the size of rectTesting
    rectScaled.x = 50;
    rectScaled.width = 200;
    rectScaled.y = 50;
    rectScaled.height = 200;

    scaleFactor = 2;
    rectTransformations.scale(rectTesting, scaleFactor);
    ASSERT_THAT(rectTesting, testing::Eq(rectScaled));
}

//Test scaling the rectangle to a smaller size (0 < scaleFactor < 1)
TEST_F(RectTransformationsScale, Scale_LessThanOne) {
    //Create a rectangle with half the size of rectTesting
    rectScaled.x = 125;
    rectScaled.width = 50;
    rectScaled.y = 125;
    rectScaled.height = 50;

    scaleFactor = 0.5;
    rectTransformations.scale(rectTesting, scaleFactor);
    ASSERT_THAT(rectTesting, testing::Eq(rectScaled));
}

//Test scaling the rectangle to a zero size (scaleFactor = 0)
TEST_F(RectTransformationsScale, ScaleRectangle_Zero) {
    //Create a rectangle with no area, in the middle of rectTesting
    rectScaled.x = 150;
    rectScaled.width = 0;
    rectScaled.y = 150;
    rectScaled.height = 0;

    scaleFactor = 0.0;
    rectTransformations.scale(rectTesting, scaleFactor);
    ASSERT_THAT(rectTesting, testing::Eq(rectScaled));
}

//Test scaling the rectangle to a negative size (scaleFactor < 0)
TEST_F(RectTransformationsScale, Scale_ScaleMustNotBeNegative) {
    //Create a rectangle with half the size of rectTesting

    scaleFactor = -1.0;
    ASSERT_ANY_THROW(rectTransformations.scale(rectTesting, scaleFactor));
}


class RectTransformationsRelativeScale : public ::testing::Test
{
public:

    lcg_face_attention::RectTransformations rectTransformations;
    cv::Rect rectangleToScale, scaledRectangle, expectedRect;
    cv::Rect_<float> desiredRelativeRectangle;
    double scaleFactor;

    void SetUp(){
        rectangleToScale.width = 100;
        rectangleToScale.height = 100;
        rectangleToScale.x = 100;
        rectangleToScale.y = 100;
    }
};

//Test changing the searching area to the same size (expectedRect [100 x 100 from (100, 100)])
TEST_F(RectTransformationsRelativeScale, RelativeScale_Equal) {
    //Create a rectangle with the same size of rectangleToScale
    expectedRect.width = 100;
    expectedRect.height = 100;
    expectedRect.x = 100;
    expectedRect.y = 100;

    //Create a rectangle that scalates to the same size of expectedRect
    desiredRelativeRectangle.width = 1.0;
    desiredRelativeRectangle.height = 1.0;
    desiredRelativeRectangle.x = 0.0;
    desiredRelativeRectangle.y = 0.0;

    scaledRectangle = rectTransformations.relativeScale(rectangleToScale, desiredRelativeRectangle);
    ASSERT_THAT(scaledRectangle, testing::Eq(expectedRect));
}

//Test changing the searching area to the upper half (expectedRect [100 x 50 from (100, 100)])
TEST_F(RectTransformationsRelativeScale, RelativeScale_UpperHalf) {
    //Create a rectangle with the same size of rectangleToScale
    expectedRect.width = 100.0;
    expectedRect.height = 50.0;
    expectedRect.y = 100.0;
    expectedRect.x = 100.0;

    //Create a rectangle that scalates to the same size of expectedRect
    desiredRelativeRectangle.width = 1.0;
    desiredRelativeRectangle.height = 0.5;
    desiredRelativeRectangle.x = 0.0;
    desiredRelativeRectangle.y = 0.0;

    scaledRectangle = rectTransformations.relativeScale(rectangleToScale, desiredRelativeRectangle);
    ASSERT_THAT(scaledRectangle, testing::Eq(expectedRect));
}

//Test changing the searching area to the lower half (expectedRect [100 x 50 from (100, 150)])
TEST_F(RectTransformationsRelativeScale, RelativeScale_LowerHalf) {
    //Create a rectangle with the same size of rectangleToScale
    expectedRect.width = 100.0;
    expectedRect.height = 50.0;
    expectedRect.x = 100.0;
    expectedRect.y = 150.0;

    //Create a rectangle that scalates to the same size of expectedRect
    desiredRelativeRectangle.width = 1.0;
    desiredRelativeRectangle.height = 0.5;
    desiredRelativeRectangle.x = 0.0;
    desiredRelativeRectangle.y = 0.5;

    scaledRectangle = rectTransformations.relativeScale(rectangleToScale, desiredRelativeRectangle);
    ASSERT_THAT(scaledRectangle, testing::Eq(expectedRect));
}

//Test changing the searching area to the left half (expectedRect [50 x 100 from (100, 100)])
TEST_F(RectTransformationsRelativeScale, RelativeScale_LeftHalf) {
    //Create a rectangle with the same size of rectangleToScale
    expectedRect.width = 50.0;
    expectedRect.height = 100.0;
    expectedRect.x = 100.0;
    expectedRect.y = 100.0;

    //Create a rectangle that scalates to the same size of expectedRect
    desiredRelativeRectangle.width = 0.5;
    desiredRelativeRectangle.height = 1.0;
    desiredRelativeRectangle.x = 0.0;
    desiredRelativeRectangle.y = 0.0;

    scaledRectangle = rectTransformations.relativeScale(rectangleToScale, desiredRelativeRectangle);
    ASSERT_THAT(scaledRectangle, testing::Eq(expectedRect));
}

//Test changing the searching area to the right half (expectedRect [50 x 100 from (150, 100)])
TEST_F(RectTransformationsRelativeScale, RelativeScale_RightHalf) {
    //Create a rectangle with the same size of rectangleToScale
    expectedRect.width = 50.0;
    expectedRect.height = 100.0;
    expectedRect.x = 150.0;
    expectedRect.y = 100.0;

    //Create a rectangle that scalates to the same size of expectedRect
    desiredRelativeRectangle.width = 0.5;
    desiredRelativeRectangle.height = 1.0;
    desiredRelativeRectangle.x = 0.5;
    desiredRelativeRectangle.y = 0.0;

    scaledRectangle = rectTransformations.relativeScale(rectangleToScale, desiredRelativeRectangle);
    ASSERT_THAT(scaledRectangle, testing::Eq(expectedRect));
}

//Test changing the searching area outside it (desiredRelativeRectangle.element > 1.0 or < 0.0)
TEST_F(RectTransformationsRelativeScale, RelativeScale_DesiredSearchingAreaOutside) {
    //Create a rectangle that scalates same size of rectangleToScale
    desiredRelativeRectangle.width = 0.0;
    desiredRelativeRectangle.height = 0.0;
    desiredRelativeRectangle.x = 1.5;
    desiredRelativeRectangle.y = 0.0;

    ASSERT_ANY_THROW(rectTransformations.relativeScale(rectangleToScale, desiredRelativeRectangle););
}

class RectTransformationsAbsoluteScaleList : public ::testing::Test
{
public:

    lcg_face_attention::RectTransformations rectTransformations;
    std::vector<cv::Rect> expectedVector, testVector;
    cv::Rect expectedRect1, expectedRect2, testRect1, testRect2;
    short int scale;

    void SetUp(){
    }
};

//Test scaling one rectangle
TEST_F(RectTransformationsAbsoluteScaleList, AbsoluteScaleList_OneRectangle) {
    expectedRect1.width = 200;
    expectedRect1.height = 200;
    expectedRect1.x = 0;
    expectedRect1.y = 0;
    expectedVector.push_back(expectedRect1);

    testRect1.width = 100;
    testRect1.height = 100;
    testRect1.x = 0;
    testRect1.y = 0;
    testVector.push_back(testRect1);

    scale = 2;

    rectTransformations.absoluteScale(testVector, scale);
    ASSERT_THAT(testVector, testing::Eq(expectedVector));
}

//Test scaling more than one rectangle
TEST_F(RectTransformationsAbsoluteScaleList, AbsoluteScaleList_TwoRectangles) {
    expectedRect1.width = 200;
    expectedRect1.height = 200;
    expectedRect1.x = 0;
    expectedRect1.y = 0;
    expectedVector.push_back(expectedRect1);

    expectedRect2.width = 400;
    expectedRect2.height = 400;
    expectedRect2.x = 200;
    expectedRect2.y = 200;
    expectedVector.push_back(expectedRect2);

    testRect1.width = 100;
    testRect1.height = 100;
    testRect1.x = 0;
    testRect1.y = 0;
    testVector.push_back(testRect1);

    testRect2.width = 200;
    testRect2.height = 200;
    testRect2.x = 100;
    testRect2.y = 100;
    testVector.push_back(testRect2);

    scale = 2;

    rectTransformations.absoluteScale(testVector, scale);
    ASSERT_THAT(testVector, testing::Eq(expectedVector));
}
