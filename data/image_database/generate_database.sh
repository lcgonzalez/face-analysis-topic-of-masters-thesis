#!/bin/bash

echo "Building the category image database."
categoryList="urls/lists.txt"
echo "Reading categories from: $categoryList"

imageName="image"
extension=".jpg"
minimumsize=100000
maxImages=50
maxTimeout=8 # Seconds
maxTries=2 
#source http://www.image-net.org/synset?wnid=n03079741


function downloadImages {
urlsFile_=$1
baseDir_=$2

n_=1

cat $urlsFile_ | while read line_
do
  if [ "$line_" != "" ]; then
    echo Downloading $line_
    file_="$baseDir_/$imageName$n_$extension"
    wget -t $maxTimeout -T $maxTries -q "$line_" -O $file_
    actualsize_=$(du -b "$file_" | cut -f 1)
    if [ $actualsize_ -ge $minimumsize ]; then
      #echo size is over $minimumsize bytes, the image is valid
      n_=$(echo "$n_+1" | bc)
    else
      rm $file_
      #echo size is under $minimumsize bytes, image deleted
    fi
    if [ $n_ -gt $maxImages ]; then
      break
    fi
  fi
done
}


urlsFileExtension=".txt"
cat $categoryList | while read line
do
  if [ "$line" != "" ]; then
    echo Processing the category: $line
    urlsFile="urls/$line$urlsFileExtension"
    baseDir=$line
    mkdir $baseDir
    downloadImages $urlsFile $baseDir
  fi
done

