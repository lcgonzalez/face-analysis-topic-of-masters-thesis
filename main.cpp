// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/**
//  @file main.cpp
//  @author Luis Gonzalez <lc.gonzalez23@gmail.com>
//  @version 1.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#include <iostream>
#include <stdio.h>
#include <map>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/detail/file_parser_error.hpp>
#include <boost/version.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "attentionactioner.h"
#include "eyeattentiondetector.h"
#include "enhancedcascadeclassifier.h"
#include "faceattentiondetector.h"
#include "headposeclassifier.h"
#include "imagedatabase.h"
#include "settings.h"
#include "vambroker.h"

#define BOOST_LEXICAL_CAST_ASSUME_C_LOCALE 1 // For lexical_cast

void version()
{
    using std::cout;
    using std::endl;
    cout << "Face attention detection 0.2" << endl;
    cout << "Using OpenCV version " << CV_VERSION << endl;
    cout << "Using Boost version " << BOOST_LIB_VERSION << endl;
}

void help()
{
    using std::cout;
    using std::endl;
    cout << endl
         << " This programs demonstrates the use of the Face Analysis and Attention Detector, which" << endl
         << " goal is to determine whether or not a human is giving attention to computer screen," << endl
         << " using a webcam as the sensor, and acting (displaying images) according to that level" << endl
         << " of attention, so that the user don't get bored." << endl
         << endl
         << " Usage:" << endl
         << " ./face-attention-detection [--config-file[=<config_file_path>]] " << endl
         << " " << endl
         << " The options are:" << endl
         << "   config-file     This is the configuration file where all the parameters are set" << endl
         << endl
         << " The commands are:" << endl
         << "   --help          Displays this help dialog" << endl
         << "   --license       Displays the license agreement" << endl
         << "   --version       Displays the verion of this program and dependencies" << endl
         << endl;

    //version();
}

void license()
{
    using std::cout;
    using std::endl;
    cout << "  By downloading, copying, installing or using the software you agree to this license." << endl
         << "  If you do not agree to this license, do not download, install, copy or" << endl
         << "  use the software." << endl
         << endl
         <<  "                         License Agreement                                          " << endl
         <<  "               For the Face Analysis and Attention Detector                         " << endl
         <<  endl
         << " Copyright (c) 2013, Luis Carlos González García, all rights reserved." << endl
         << " Third party copyrights are property of their respective owners." << endl
         << endl
         << " Redistribution and use in source and binary forms, with or without" << endl
         << " modification, are permitted provided that the following conditions are met:" << endl
         << endl
         << "   1. Redistributions of source code must retain the above copyright notice," << endl
         << "      this list of conditions and the following disclaimer." << endl
         << endl
         << "   2. Redistributions in binary form must reproduce the above copyright notice," << endl
         << "      this list of conditions and the following disclaimer in the documentation" << endl
         << "      and/or other materials provided with the distribution." << endl
         << endl
         << "   3. Neither the name of the CINVESTAV, nor the names of its contributors may" << endl
         << "      be used to endorse or promote products derived from this software without" << endl
         << "      specific prior written permission." << endl
         << endl
         << " THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND" << endl
         << " ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED" << endl
         << " WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE" << endl
         << " DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE" << endl
         << " FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL" << endl
         << " DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR" << endl
         << " SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER" << endl
         << " CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY," << endl
         << " OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE" << endl
         << " OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE." << endl;
}

void unknownOption(std::string optionName)
{
    using std::cout;
    using std::endl;
    cout << "face-attention-detection: " << optionName << " is not a command. "
         << "See 'face-attention-detection --help'." << endl;
}

bool isDigits(const std::string &str)
{
    return str.find_first_not_of("0123456789") == std::string::npos;
}

int main( int argc, const char** argv )
{
    using std::cerr;
    using std::cout;
    using std::endl;

    using lcg_face_attention::Settings;
    using lcg_face_attention::EnhancedCascadeClassifier;
    using lcg_face_attention::HeadPoseClassifier;
    using lcg_face_attention::ImageLogger;
    using lcg_face_attention::ImageDatabase;
    using lcg_face_attention::EyeAttentionDetector;
    using lcg_face_attention::VAMBroker;

    cv::VideoCapture capture;
    cv::Mat frame, frameCopy, image;
    const std::string configOption = "--config-file=";
    size_t configOptionLength = configOption.length();
    const std::string helpOption = "--help";
    size_t helpOptiontLength = helpOption.length();
    const std::string versionOption = "--version";
    size_t versionOptionLength = versionOption.length();
    const std::string licenseOption = "--license";
    size_t licenseOptionLength = licenseOption.length();
    std::string inputName;

    Settings settings;
    // If no arguments were introduced
    if(argc == 1) {
        help();
        return 0;
    }
    // Read command line arguments
    for( int i = 1; i < argc; i++ )
    {
        if( helpOption.compare( 0, helpOptiontLength, argv[i], helpOptiontLength ) == 0 ) {
            help();
            return 0;
        }
        else if( versionOption.compare( 0, versionOptionLength, argv[i], versionOptionLength ) == 0 ) {
            version();
            return 0;
        }
        else if( licenseOption.compare( 0, licenseOptionLength, argv[i], licenseOptionLength ) == 0 ) {
            license();
            return 0;
        }
        else if( configOption.compare( 0, configOptionLength, argv[i], configOptionLength ) == 0) {
            // Configuration file specified
            try {
                settings.readSettings( argv[i] + configOptionLength );
            }
            catch(boost::property_tree::file_parser_error &e) {
                cerr << "Exception rised: " << e.what() << endl;
                return 0;
            }
        } else {
            unknownOption(argv[i]);
            return 0;
        }
    }
    settings.log();

    cout << endl << " Hit any key to quit." << endl;

    EnhancedCascadeClassifier faceCascade, leftProfileFaceCascade, rightProfileFaceCascade,
            rightEyeCascade, leftEyeCascade, mouthCascade;

    // Load classifier files
    if( !faceCascade.load( settings.getParameter("run.classifiers.frontal_face") ) ) {
        cerr << "WARNING: Could not load classifier cascade for frontal face" << endl;
        return 1;
    }
    if( !leftProfileFaceCascade.load( settings.getParameter( "run.classifiers.profile_face" ) ) )
        cerr << "WARNING: Could not load classifier cascade for profile face" << endl;
    if( !rightEyeCascade.load( settings.getParameter( "run.classifiers.right_eye" ) ) )
        cerr << "WARNING: Could not load classifier cascade for right eye" << endl;
    if( !leftEyeCascade.load( settings.getParameter( "run.classifiers.left_eye" ) ) )
        cerr << "WARNING: Could not load classifier cascade for left eye" << endl;
    if( !mouthCascade.load( settings.getParameter( "run.classifiers.mouth" ) ) )
        cerr << "WARNING: Could not load classifier cascade for the mouth" << endl;
    inputName = settings.getParameter("run.video_path");

    double scale = boost::lexical_cast<int, std::string>(settings.getParameter("run.scale"));
    if( scale < 1)
        cerr << "WARNING: Wrong scale, the scale should be > 1" << endl;


    //If the video source is a camera
    if( inputName.empty() || isDigits(inputName) )
    {
        // 900 is the value of CV_CAP_OPENNI macro, this value needs to be used on the
        // settings.config file in order to choose the kinect or compatible camera
        int c = atoi(inputName.c_str());
        capture.open(c);
        if(!capture.isOpened()) cout << "Capture from CAM " << c << " didn't work" << endl;
    }
    else if( inputName.size() ) //If the video source is a file
    {
        image = cv::imread( inputName, 1 );
        if( image.empty() )
        {
            capture.open(inputName.c_str());
            if(!capture.isOpened()) cout << "Capture from AVI didn't work" << endl;
        }
    }
    else
    {
        image = cv::imread( "lena.jpg", 1 );
        if(image.empty()) cout << "Couldn't read lena.jpg" << endl;
    }


    EnhancedCascadeClassifier rotatedLeftFrontalFaceCascade, rotatedRightFrontalFaceCascade;

    rotatedLeftFrontalFaceCascade = faceCascade;
    rotatedLeftFrontalFaceCascade.setRotationAngle(45);
    rotatedRightFrontalFaceCascade = faceCascade;
    rotatedRightFrontalFaceCascade.setRotationAngle(-45);

    rightProfileFaceCascade = leftProfileFaceCascade;
    rightProfileFaceCascade.setMirror(true);

    faceCascade.setName("Frontal face");
    leftProfileFaceCascade.setName("Left profile");
    rightProfileFaceCascade.setName("Right profile");
    rotatedLeftFrontalFaceCascade.setName("45° rotation");
    rotatedRightFrontalFaceCascade.setName("-45° rotation");
    leftEyeCascade.setName("Left Eye");
    rightEyeCascade.setName("Right Eye");

    HeadPoseClassifier* headClassifier = new HeadPoseClassifier;
    headClassifier->addClassifier(faceCascade);
    //headClassifier->addClassifier(leftProfileFaceCascade);
    //headClassifier->addClassifier(rightProfileFaceCascade);
    //headClassifier->addClassifier(rotatedLeftFrontalFaceCascade);
    //headClassifier->addClassifier(rotatedRightFrontalFaceCascade);
    headClassifier->setScale(scale);

    EyeAttentionDetector* eyeAttention = new EyeAttentionDetector();

    //--------->Define searching areas within the face (fixed to face geometry)
    cv::Rect_<float> relativeSearchingArea;
    //Desired relative area for the left eye (left upper corner of the face)
    relativeSearchingArea.width = 0.6;
    relativeSearchingArea.height = 0.5;
    relativeSearchingArea.x = 1.0 - relativeSearchingArea.width;
    relativeSearchingArea.y = 0.0;
    leftEyeCascade.setSearchArea(relativeSearchingArea);
    eyeAttention->pushClassifier(leftEyeCascade);

    //Desired relative area for the right eye (right upper corner of the face)
    relativeSearchingArea.width = 0.6;
    relativeSearchingArea.height = 0.5;
    relativeSearchingArea.x = 0.0;
    relativeSearchingArea.y = 0.0;
    rightEyeCascade.setSearchArea(relativeSearchingArea);
    eyeAttention->pushClassifier(rightEyeCascade);

    lcg_face_attention::FaceAttentionDetector* faceAttentionDetector
            = new lcg_face_attention::FaceAttentionDetector();
    faceAttentionDetector->setHeadPoseClassifier(headClassifier);
    faceAttentionDetector->setEyeAttentionDetector(eyeAttention);

    ImageLogger* pImageLoger = new ImageLogger;
    faceAttentionDetector->setImageLogger(pImageLoger);


    VAMBroker broker(faceAttentionDetector); //Connect callback
    broker.setImageLogger(pImageLoger);

    // load image database paths
    ImageDatabase* imageDatabase = new ImageDatabase;
    imageDatabase->loadFiles("../data/image_database/");
    lcg_face_attention::ActionerInterface* attentionActioner
            = new lcg_face_attention::AttentionActioner();
    lcg_face_attention::AttentionActioner* attActioner =
            dynamic_cast<lcg_face_attention::AttentionActioner*>(attentionActioner);
    attActioner->setImageDatabase(imageDatabase);
    attActioner->setResultsFilename(settings.getParameter("run.experiments.results_filename"));
    attActioner->setMaxDataEntries(
                boost::lexical_cast<long long int, std::string>(settings.getParameter("run.experiments.data_entries")));
    broker.setActioner(attentionActioner);

    // Load attention rules
    std::map<std::string, bool> rules;
    rules["face"] = boost::lexical_cast<bool, std::string>(settings.getParameter("run.attention_rules.face"));
    rules["eyes"] = boost::lexical_cast<bool, std::string>(settings.getParameter("run.attention_rules.eyes"));
    rules["iris"] = boost::lexical_cast<bool, std::string>(settings.getParameter("run.attention_rules.iris"));
    broker.setAttentionRules(rules);

    cv::Mat depthMap, bgrImage;
    if( capture.isOpened() )
    {
        cout << "In capture ..." << endl;

        for(;;)
        {

            capture.grab();
            capture.retrieve( depthMap, CV_CAP_OPENNI_DEPTH_MAP ); //Depth map provided by the kinect (OpenNI)
            capture.retrieve( bgrImage, CV_CAP_OPENNI_BGR_IMAGE );

            if(depthMap.empty() || bgrImage.empty()) {
                break;
            }

            bgrImage.copyTo( frameCopy );
            //faces = headClassifier.detect(frameCopy);
            //eyeAttention->detectAttention(frameCopy, faces);
            broker.onImageAcquired(frameCopy);

            if( cv::waitKey( 10 ) >= 0 ) // Press any key to exit
                capture.release();
        }
        //cv::waitKey(0); // Pause last image unitl any key is pressed
    }
    else
    {
        cout << "In image read" << endl;
        if( !image.empty() )
        {
            broker.onImageAcquired(image);
            cv::waitKey(0);
        }
        else if( !inputName.empty() )
        {
            /* assume it is a text file containing the
            list of the image filenames to be processed - one per line */
            FILE* f = fopen( inputName.c_str(), "rt" );
            if( f )
            {
                char buf[1000+1];
                while( fgets( buf, 1000, f ) )
                {
                    int len = static_cast<int>(strlen(buf)), c;
                    while( len > 0 && isspace(buf[len-1]) )
                        len--;
                    buf[len] = '\0';
                    cout << "file " << buf << endl;
                    image = cv::imread( buf, 1 );
                    if( !image.empty() )
                    {
                        broker.onImageAcquired(image);
                        c = cv::waitKey(0);
                        if( c == 27 || c == 'q' || c == 'Q' )
                            break;
                    }
                    else
                    {
                        cerr << "Snap, couldn't read image " << buf << endl;
                    }
                }
                fclose(f);
            }
        }
    }

    cv::destroyWindow("result");

    return 0;
}
