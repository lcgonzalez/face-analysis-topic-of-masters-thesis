#include "imagelogger.h"

namespace lcg_face_attention {

ImageLogger::ImageLogger() :
    textRow(0)
{

}

void ImageLogger::drawTitle(std::string text, cv::Mat &img)
{
    int fontFace = cv::FONT_HERSHEY_SIMPLEX;
    double fontScale = 1;
    int thickness = 2;

    int baseline=0;
    cv::Size textSize = cv::getTextSize(text, fontFace,
                                fontScale, thickness, &baseline);
    baseline += thickness;

    // center the text
    //Point textOrg((img.cols - textSize.width)/2,
    //              (img.rows + textSize.height)/2);
    cv::Point textOrg(0, textSize.height);

    // draw the box
    cv::rectangle(img, textOrg + cv::Point(0, baseline),
              textOrg + cv::Point(textSize.width, -textSize.height),
              cv::Scalar(0,0,255));

    // then put the text itself
    cv::putText(img, text, textOrg, fontFace, fontScale,
            cv::Scalar::all(0), thickness, 8);
}

void ImageLogger::drawText(std::string text, cv::Mat &img)
{
    int fontFace = cv::FONT_HERSHEY_SIMPLEX;
    double fontScale = .5;
    int thickness = 1;

    int baseline=0;
    cv::Size textSize = cv::getTextSize(text, fontFace,
                                fontScale, thickness, &baseline);
    baseline += thickness;

    // center the text
    //Point textOrg((img.cols - textSize.width)/2,
    //              (img.rows + textSize.height)/2);
    cv::Point textOrg(img.cols - textSize.width, textSize.height + textSize.height*textRow++);

    // draw the box
    //rectangle(img, textOrg + Point(0, baseline),
    //          textOrg + Point(textSize.width, -textSize.height),
    //          Scalar(0,0,255));

    // then put the text itself
    cv::putText(img, text, textOrg, fontFace, fontScale,
            cv::Scalar::all(0), thickness, 8);
}

void ImageLogger::resetTextRowToBegining()
{
    textRow = 0;
}

} // namespace lcg_face_attention
