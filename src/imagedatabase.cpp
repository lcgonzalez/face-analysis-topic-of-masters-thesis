#include "imagedatabase.h"

#include<iostream>
#include<stdlib.h> //srand

#include <boost/algorithm/string/case_conv.hpp>
#include <boost/filesystem.hpp>

namespace lcg_face_attention {


ImageDatabase::ImageDatabase()
{
    ext.push_back(".jpg");
    ext.push_back(".png");
    srand ( time(NULL) ); //seed pseudorandom number generator with time
}

void ImageDatabase::loadFiles(std::string path)
{
    /*
    // Alternative dirent.h implementation
    std::cout << "inside loadFiles" << std::endl;
    DIR *dir;
    struct dirent *ent;
    if( (dir = opendir(path.c_str())) != NULL) {
        /* print all files and directories within a directory /
        while( (ent = readdir(dir)) != NULL) {
            std::cout << ent->d_name << std::endl;
            std::cout << ent->d_type << std::endl;
        }
        closedir(dir);
    } else {
        /* could not open directory /
        std::cerr << "" << std::endl;
        perror("opendir");
    }
    */

    for ( boost::filesystem::recursive_directory_iterator it( boost::filesystem::path(path.c_str()) );
        it != boost::filesystem::recursive_directory_iterator(); ++it )
    {
        if ( boost::filesystem::is_regular_file( it->status() ) )
        {
            // Get file extension and store path if it's an image
            std::string extension(boost::algorithm::to_lower_copy(it->path().extension().string()));
            for(std::vector<std::string>::iterator vecIt = ext.begin(); vecIt != ext.end();
                vecIt++) {
                if(*vecIt == extension) {
                    files.push_back( it->path().string() );
                    //std::cout << it->path().string() << std::endl;
                    //files.push_back( it->path().filename().string() );
                }
            }
        }
    }

    #ifndef NDEBUG      //-----------Debugging code
        std::cout << "ImageDatabase: Number of image files: " << files.size() << std::endl;
        std::copy( files.begin(), files.end(), std::ostream_iterator<std::string>( std::cout, "\n" ) );
    #endif              //-----------End of debugging code


}

std::string ImageDatabase::getImage()
{
    int x = rand() % files.size();
    return files.at(x);
}

} // namespace lcg_face_attention
