#include "recttransformations.h"

#include <stdexcept>
#include <typeinfo>

namespace lcg_face_attention {

RectTransformations::RectTransformations()
{
}

void RectTransformations::scale(cv::Rect &rRectangleToScale, const float kScale) const
{
    if( kScale >= 0) {
        rRectangleToScale.x -= (kScale-1)*rRectangleToScale.width*.5;
        rRectangleToScale.width *= kScale;
        rRectangleToScale.y -= (kScale-1)*rRectangleToScale.height*.5;
        rRectangleToScale.height *= kScale;
    } else { //scale not valid
        std::stringstream ss;
        ss << "The scale value of \"" << kScale << "\" used in: \""
           << typeid(this).name() << "::" << __FUNCTION__
           << "\" can't be nagattive!" << std::endl;
        throw std::invalid_argument(ss.str());
    }
}

cv::Rect RectTransformations::relativeScale(const cv::Rect rRectangleToScale, cv::Rect_<float> relativeRectangle) const
{
    cv::Rect tmpRect = rRectangleToScale;
    tmpRect.x += static_cast<float>(rRectangleToScale.width) * relativeRectangle.x;
    tmpRect.y += static_cast<float>(rRectangleToScale.height) * relativeRectangle.y;
    tmpRect.width = static_cast<float>(rRectangleToScale.width) * relativeRectangle.width;
    tmpRect.height = static_cast<float>(rRectangleToScale.height) * relativeRectangle.height;

    //Check to see of the new searching area is invalid (Trying to access
    //outside the searching area boundaries)
    bool validArea = true;
    if((tmpRect.x < rRectangleToScale.x) || (tmpRect.y < rRectangleToScale.y)
            || ((tmpRect.width + tmpRect.x) > (rRectangleToScale.width + rRectangleToScale.x))
            || ((tmpRect.height + tmpRect.y) > (rRectangleToScale.height + rRectangleToScale.y))) {
        validArea = false;
    }

    using std::endl;
    if(!validArea) {
        std::stringstream tempSs;
        tempSs << "Trying to relative resize the rectangle outside the original "
               << "rectangle boundaries!" << endl
               << "Actual Rectangle to scale is: " << rRectangleToScale << endl
               << "But trying to resize to rectangle: " << tmpRect << endl
               << "Using the relative rectangle: " << relativeRectangle << endl;
        throw std::invalid_argument(tempSs.str());
    }

    return tmpRect;
}

void RectTransformations::absoluteScale(cv::Rect &rRectangleToScale, const short kScale)
{
    rRectangleToScale.x *= kScale;
    rRectangleToScale.y *= kScale;
    rRectangleToScale.height *= kScale;
    rRectangleToScale.width *= kScale;
}

void RectTransformations::absoluteScale(std::vector<cv::Rect> &rRectanglesToScaleList, const short kScale)
{
    // Scale the whole vector of rectangles
    for(std::vector<cv::Rect>::iterator rectIt = rRectanglesToScaleList.begin();
        rectIt != rRectanglesToScaleList.end(); rectIt++) {
        rectIt->x *= kScale;
        rectIt->y *= kScale;
        rectIt->height *= kScale;
        rectIt->width *= kScale;
    }
}

} // namespace lcg_face_attention
