// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#include "enhancedcascadeclassifier.h"

#include <sstream>
#include <stdexcept>

namespace lcg_face_attention {

EnhancedCascadeClassifier::EnhancedCascadeClassifier()
    : CascadeClassifier::CascadeClassifier(),
      rotateImage_(false),
      imageRotationAngle_(0),
      relativeArea_(cv::Point(0,0), cv::Point(100,100)), //Default, search on the entire searching area

      // CascadeClassifier::detectMultiScale method default parameters
      scaleFactor_(1.1),
      minNeighbors_(2),
      flags_(0
      | CV_HAAR_FIND_BIGGEST_OBJECT
      | CV_HAAR_DO_ROUGH_SEARCH
      //| CV_HAAR_DO_CANNY_PRUNING
      //| CV_HAAR_SCALE_IMAGE
            ),
      minSize_(cv::Size(30, 30)),
      maxSize_(cv::Size())
{

}

cv::Rect EnhancedCascadeClassifier::resizeSearchArea(const cv::Rect searchArea) const
{
    return rectTransformations_.relativeScale(searchArea, relativeArea_);
}

} // namespace lcg_face_attention

