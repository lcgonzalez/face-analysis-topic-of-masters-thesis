#include "faceattentiondetector.h"

#include <opencv2/highgui/highgui.hpp>
#include <iostream>

namespace lcg_face_attention {

FaceAttentionDetector::FaceAttentionDetector() :
    pEyeAttentionDetector_(NULL),
    pHeadPoseClassifier_(NULL),
    pImageLogger_(NULL)
{
}

FaceAttentionDetector::~FaceAttentionDetector()
{
    delete pEyeAttentionDetector_;
    delete pHeadPoseClassifier_;
}

bool FaceAttentionDetector::detect(cv::Mat &rImage, const std::vector<cv::Rect> kSearchingAreas)
{
    pImageLogger_->resetTextRowToBegining();  // Start writing on top of the image
    std::vector<cv::Rect> faces;
    faces = pHeadPoseClassifier_->detect(rImage);
    pEyeAttentionDetector_->detect(rImage, faces);
}

AttentionInterface::AttentionConnection FaceAttentionDetector::connect(const AttentionSignal::slot_function_type kSubscriber )
{
    eyeAttentionDetector_connection = pEyeAttentionDetector_->connect( kSubscriber );
    return AttentionEvent_signal_.connect( kSubscriber );
}

void FaceAttentionDetector::disconnect(const AttentionConnection kSubscriberConnection )
{
    pEyeAttentionDetector_->disconnect(eyeAttentionDetector_connection);
    kSubscriberConnection.disconnect();
}

void FaceAttentionDetector::setEyeAttentionDetector(EyeAttentionDetector *pNewEyeAttentionDetector)
{
    pEyeAttentionDetector_ = pNewEyeAttentionDetector;
    updateImageLogger();
}

void FaceAttentionDetector::setHeadPoseClassifier(HeadPoseClassifier *pNewHeadPoseClassifier)
{
    pHeadPoseClassifier_ = pNewHeadPoseClassifier;
    updateImageLogger();
}

void FaceAttentionDetector::setImageLogger(ImageLogger *pNewImageLogger)
{
    pImageLogger_ = pNewImageLogger;
    updateImageLogger();
}

void FaceAttentionDetector::updateImageLogger()
{
    if(pImageLogger_) {
        if( pEyeAttentionDetector_ ) {
            pEyeAttentionDetector_->setImageLogger(pImageLogger_);
        }
        if( pHeadPoseClassifier_ ) {
            pHeadPoseClassifier_->setImageLogger(pImageLogger_);
        }
    }
}

} // namespace lcg_face_attention
