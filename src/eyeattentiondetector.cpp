// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#include "eyeattentiondetector.h"

#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace lcg_face_attention {

EyeAttentionDetector::EyeAttentionDetector() :
    attention_( false ),
    pAttentionEvent_( new AttentionEvent() ),
    pImageLogger_()
{
    generateColors();
}

EyeAttentionDetector::~EyeAttentionDetector()
{
    delete pColors_;
    delete pAttentionEvent_;
}

void EyeAttentionDetector::generateColors()
{
    // Defined colors useful for drawing areas when debbuging
    #if __cplusplus > 199711L   // c++11 support
    pColors_ = new cv::Scalar[8] {
            CV_RGB(0,0,255),
            CV_RGB(0,128,255),
            CV_RGB(0,255,255),
            CV_RGB(0,255,0),
            CV_RGB(255,128,0),
            CV_RGB(255,255,0),
            CV_RGB(255,0,0),
            CV_RGB(255,0,255) };

    #else                       // c++97 support
    pColors_ = new cv::Scalar[8];
    pColors_[0] = CV_RGB(0,0,255);
    pColors_[1] = CV_RGB(0,128,255);
    pColors_[2] = CV_RGB(0,255,255);
    pColors_[3] = CV_RGB(0,255,0);
    pColors_[4] = CV_RGB(255,128,0);
    pColors_[5] = CV_RGB(255,255,0);
    pColors_[6] = CV_RGB(255,0,0);
    pColors_[7] = CV_RGB(255,0,255);
    #endif
}

AttentionInterface::AttentionConnection EyeAttentionDetector::connect(const AttentionSignal::slot_function_type kSubscriber )
{
    return AttentionEvent_signal_.connect( kSubscriber );
}

void EyeAttentionDetector::disconnect(const AttentionConnection kSubscriber )
{
    kSubscriber.disconnect();
}

void EyeAttentionDetector::pushClassifier(const EnhancedCascadeClassifier &rLeftEyeCascade )
{
    classifierList_.push_back( rLeftEyeCascade );
}

void EyeAttentionDetector::setImageLogger(ImageLogger *pNewImageLogger)
{
    pImageLogger_ =  pNewImageLogger;
}

ImageLogger* EyeAttentionDetector::getImageLogger()
{
    return pImageLogger_;
}

bool EyeAttentionDetector::detect(cv::Mat& rFaceImage,
                                  const std::vector<cv::Rect> kFacesList)
{
    // If face were not detected, return.
    if(kFacesList.size() == 0) {
        attention_ = false;
        pAttentionEvent_->setAttention(false);
        pAttentionEvent_->setOrigin(AttentionEvent::FACE_NOT_DETECTED);
        AttentionEvent_signal_(pAttentionEvent_);
        return attention_;
    }

    // For each face or primary object detected
    for( std::vector<cv::Rect>::const_iterator rectIt = kFacesList.begin();
         rectIt != kFacesList.end(); rectIt++)
    {
        cv::Rect faceBoundingBox = *rectIt;

        //-----------Debugging code
        #ifndef NDEBUG
        //Draw face bounding box rectangle
        rectangle(rFaceImage, faceBoundingBox, pColors_[0], 2, 8, 0);
        #endif
        //-----------End of debugging code

        //Detect eyes on the face
        std::vector<cv::Rect> eyesDetected;
        for(std::vector<EnhancedCascadeClassifier>::iterator cascadeIt = classifierList_.begin();
            cascadeIt != classifierList_.end(); cascadeIt++) {
            EnhancedCascadeClassifier tmpClassifier = *cascadeIt;
            eyesDetected = detectObject(rFaceImage, tmpClassifier,
                                                        faceBoundingBox);
            if(!eyesDetected.empty())
                break;

            // TODO - move the eye detectors inside classifierList_ to
            // adapt to the most common eye detected. The last eye detected
            // should come first.
        }

        //Eyes not detected, hence the subject is not attentive to the camera
        if(eyesDetected.empty()) {
        attention_ = false;
        pAttentionEvent_->setAttention(false);
        pAttentionEvent_->setOrigin(AttentionEvent::EYE_NOT_DETECTED);
        AttentionEvent_signal_(pAttentionEvent_);

        } else {
            //For the first eye detected
            cv::Rect eyeArea = eyesDetected.back();
            rectTransformations_.scale(eyeArea, 0.8);
            detectGaze(rFaceImage, eyeArea);
        }
        return attention_;
    }
}

std::vector<cv::Rect> EyeAttentionDetector::detectObject(cv::Mat &rImage,
                                                     EnhancedCascadeClassifier &rObjectCascade,
                                                     const cv::Rect &rTotalSearchingArea)
{
    cv::Rect searchingArea = rObjectCascade.resizeSearchArea(rTotalSearchingArea);

    //-----------Debugging code
    #ifndef NDEBUG
    //----------Draw face bounding box rectangle
    cv::rectangle(rImage, searchingArea, pColors_[3], 2, 8, 0);
    #endif

    cv::Mat grayImage;
    cv::cvtColor(rImage, grayImage, CV_BGR2GRAY);

    cv::Mat searchingAreaImage = grayImage(searchingArea).clone();
    cv::equalizeHist(searchingAreaImage, searchingAreaImage);

    //--------------->Detect object on the searching area
    std::vector<cv::Rect> objectsDetected;
    //Measure detection time
    double time = 0;
    time = static_cast<double>(cv::getTickCount());
    rObjectCascade.detectMultiScale( searchingAreaImage, objectsDetected,
                                     rObjectCascade.getScaleFactor(),
                                     rObjectCascade.getMinNeighbors(),
                                     rObjectCascade.getFlags(),
                                     rObjectCascade.getMinSize(),
                                     rObjectCascade.getMaxSize() );
    time = static_cast<double>(cv::getTickCount()) - time;
    time = time/cv::getTickFrequency()*1000.0; // Time on ms

    std::cout << "Detection time, left eye = "
              << time << " ms" << std::endl;
    //Display detection time on the original image
    std::stringstream tmpStr;
    tmpStr.precision(2);
    tmpStr<<std::fixed; // for fixed point notation
    tmpStr << rObjectCascade.getName() <<": "<< time << "ms";
    pImageLogger_->drawText(tmpStr.str(),rImage);
    // NOTE - Issue #15 - Fixed logging continiously to the bottom

    //Make the dimmensions absolute
    if(!objectsDetected.empty()) {
        cv::Rect tmpRect=objectsDetected.back();
        tmpRect.x += searchingArea.x;
        tmpRect.y += searchingArea.y;

        objectsDetected.pop_back();
        objectsDetected.push_back(tmpRect);
    }

    return objectsDetected;
}

// TODO break this function in order to make easy it understanding
void EyeAttentionDetector::detectGaze(cv::Mat &rOriginalImage, const cv::Rect &rEyeArea)
{
    #ifndef NDEBUG      //-----------Debugging code
    //Draw face bounding box rectangle
    rectangle(rOriginalImage, rEyeArea, pColors_[4], 2, 8, 0);
    std::cout << "Eye absolute area: " << rEyeArea << std::endl;
    #endif              //-----------End of debugging code

    // Get image of the eye and preprocess it
    cv::Mat eyeImage = rOriginalImage(rEyeArea);
    cv::Mat preporcessedImage = preprocessEyeImage(eyeImage);

    //-----------Detect the eye edges using the Canny operator
    // Canny detector parameters
    cv::Mat detectedEdgesImage;
    int const max_lowThreshold = 100;
    int ratio = 3;
    int kernel_size = 3;
    cv::Canny( preporcessedImage, detectedEdgesImage, max_lowThreshold, max_lowThreshold*ratio, kernel_size, true );

    #ifndef NDEBUG      //-----------Debugging code
    cv::imshow( "Detected edges using the canny operator", detectedEdgesImage );
    #endif              //-----------End of debugging code

    //-----------Postprocess the imgage, using the countorus to verify iris valid position
    // findCountour parameters
    std::vector< std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours( detectedEdgesImage, contours, hierarchy, cv::RETR_TREE,
                      cv::CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
    // NOTE - Issue #1 - Fixed no contours bug (causing a segmentation fault)

    //-----------Calculate the eye iris position within the eye bounding box
    float biggestArea = 0;
    int irisIndex = -1;
    std::vector<cv::Moments> contoursMoments( contours.size() ); // Moments
    std::vector<cv::Point2f> contoursCenters( contours.size() ); // Mass centers:
    for( unsigned int i = 0; i< contours.size(); i++)
    {
        // Calculate moments and mass centers of the countours
        contoursMoments[i] = cv::moments( contours[i], false );
        contoursCenters[i] = cv::Point2f( contoursMoments[i].m10/contoursMoments[i].m00,
                                contoursMoments[i].m01/contoursMoments[i].m00 );

        // Get the biggest contour of the lower half area (or another area)
        // The bigger contour represents the iris
        if(contoursCenters[i].y > detectedEdgesImage.rows/2) {
            if(biggestArea < contoursMoments[i].m00) {
                biggestArea = contoursMoments[i].m00;
                irisIndex = i;
            }
        }
    }

    #ifndef NDEBUG      //-----------Debugging code
    if(irisIndex > -1) { // Only draw if a bigger contour (iris) was found
        // Draw bigger contour (iris)
        cv::RNG rng(12345);
        cv::Scalar color( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );

        cv::Mat drawing = cv::Mat::zeros( detectedEdgesImage.size(), CV_8UC3 );
        cv::drawContours( drawing, contours, irisIndex, color, 2, 8, hierarchy, 0, cv::Point() );

        // Show in a window
        cv::namedWindow( "Selected iris from contours", cv::WINDOW_AUTOSIZE );
        cv::imshow( "Selected iris from contours", drawing );
    }
    #endif              //-----------End of debugging code

    float rightFactor = 1.3;
    float leftFactor = 0.7;
    if(!contours.empty()) {
        if(irisIndex < 0) { // The eye iris was not found
            pImageLogger_->drawText( "Eye iris not found", rOriginalImage );
            attention_ = false;
            pAttentionEvent_->setAttention(false);
            pAttentionEvent_->setOrigin(AttentionEvent::IRIS_NOT_FOUND);
            AttentionEvent_signal_(pAttentionEvent_);

        } else if(contoursCenters[irisIndex].x > (detectedEdgesImage.cols/2)*rightFactor) {
            //The eye is looking to the right
            pImageLogger_->drawText(">>>>>>>",rOriginalImage);
            attention_ = false;
            pAttentionEvent_->setAttention(false);
            pAttentionEvent_->setOrigin(AttentionEvent::EYE_NOT_IN_CENTER);
            AttentionEvent_signal_(pAttentionEvent_);

        } else if(contoursCenters[irisIndex].x < (detectedEdgesImage.cols/2)*leftFactor) {
            //The eye is looking to the left
            pImageLogger_->drawText("<<<<<<<",rOriginalImage);
            attention_ = false;
            pAttentionEvent_->setAttention(false);
            pAttentionEvent_->setOrigin(AttentionEvent::EYE_NOT_IN_CENTER);
            AttentionEvent_signal_(pAttentionEvent_);

        } else { //The eye is looking ahead
            pImageLogger_->drawText("llllll",rOriginalImage);
            attention_ = true;
            pAttentionEvent_->setAttention(true);
            pAttentionEvent_->setOrigin(AttentionEvent::EYE_IN_CENTER);
            AttentionEvent_signal_(pAttentionEvent_);
        }
    } else { // The eye iris was not found
        pImageLogger_->drawText( "Eye iris not found", rOriginalImage );
        attention_ = false;
        pAttentionEvent_->setAttention(false);
        pAttentionEvent_->setOrigin(AttentionEvent::IRIS_NOT_FOUND);
        AttentionEvent_signal_(pAttentionEvent_);
    }
}

cv::Mat EyeAttentionDetector::preprocessEyeImage(const cv::Mat &rEyeImage) const
{
    cv::Mat eyeGrayImage, binEyeGrayImage;
    //-----------Preprocess the image using the RGB color space
    cv::cvtColor(rEyeImage, eyeGrayImage, CV_BGR2GRAY); //Turn into gray scale
    cv::equalizeHist(eyeGrayImage, eyeGrayImage); //Increase contrast
    cv::threshold(eyeGrayImage, binEyeGrayImage, 50, 255, 0); //Binarize image

    //-----------Preprocess the image using the CieLab color space
    cv::Mat eyeImageLab; //Eye image in the CieLab Space color
    cv::cvtColor(rEyeImage, eyeImageLab, CV_RGB2Lab);

    cv::Mat croppedEyesSplit[3];
    cv::split(eyeImageLab, croppedEyesSplit); //divide image in color channels
    croppedEyesSplit[1].convertTo(croppedEyesSplit[1], CV_8U);
    cv::equalizeHist(croppedEyesSplit[0], croppedEyesSplit[0]);
    cv::threshold(croppedEyesSplit[0], binEyeGrayImage, 50, 255, 0);

    // Remove noise using the Dilation-Erotion technique
    // Dilation and Erotion parameters
    int dilation_size = 2;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                                         cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                         cv::Point( dilation_size, dilation_size ) );
    cv::dilate( binEyeGrayImage, binEyeGrayImage, element);
    cv::erode( binEyeGrayImage, binEyeGrayImage, element);

    return binEyeGrayImage;
}

} // namespace lcg_face_attention
