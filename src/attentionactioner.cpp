#include "attentionactioner.h"

#include <iostream>
#include<stdlib.h> //srand

#include <opencv2/highgui/highgui.hpp>

namespace lcg_face_attention {

AttentionActioner::AttentionActioner() :
    attLength_(10),
    cb(10),
    t(0),
    bChangeImage_(false),
    iImageTime_(7),
    bDrawingAttention_(false),
    dataEntry(0),
    maxDataEntries(2)
{
    srand ( time(NULL) ); //seed pseudorandom number generator with time
    cv::namedWindow("Image1", cv::WINDOW_NORMAL);
    cv::setWindowProperty("Image1", cv::WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
    cv::moveWindow("Image1", 0, 0);

    cv::namedWindow("Image2", cv::WINDOW_NORMAL);
    cv::setWindowProperty("Image2", cv::WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
}

AttentionActioner::~AttentionActioner()
{
    delete pImageDatabase_;
    file_.close();
}

void AttentionActioner::updateAction(ActionEvent event)
{
    //boost::lock_guard<boost::mutex> guard(mtx_);
    cb.push_back(event.isRunning());
    if(t++ >= attLength_) { // Every 60 readings compute the average attention
        int averageAttention = 0;
        for(boost::circular_buffer<bool>::iterator it=cb.begin();
            it != cb.end(); it++) {
            averageAttention += *it;
        }
        t=0;
        //std::cout << "average attention: " << averageAttention << std::endl;
        if(averageAttention >= attLength_/2) { //The robot has the attention
            attention_ = true;
            //std::cout << "attenton " << std::endl;
        } else { // the robot doen't have the atttention
            attention_ = false;
            //std::cout << "noAtt " << std::endl;
        }

        if(bChangeImage_) { //if it's time to change the image
            image1 = cv::imread(pImageDatabase_->getImage(), 1);
            if(!image1.empty()) {        
                decoyImg_ = cv::Mat::zeros(image1.rows, image1.cols, CV_8UC3);
                decoyImg_.setTo(cv::Scalar(rand() % 255, rand() % 255, rand() % 255));
                logAttentionRatio();
                int x = rand() % 1000;
                // Image1 is the attentive image window
                // decoyImg_ is the distractive image window
                if(x >= 500) { // show image on attentive screen
                    cv::imshow( "Image1", image1);
                    cv::imshow( "Image2", decoyImg_);
                    bDrawingAttention_ = true;
                } else { // show image on distractive screen
                    cv::imshow( "Image2", image1);
                    cv::imshow( "Image1", decoyImg_);
                    bDrawingAttention_ = false;
                }
                bChangeImage_ = false;
            }
        }
    }
}

void AttentionActioner::action()
{
    //boost::lock_guard<boost::mutex> guard(mtx_);
    for(;;){
        bChangeImage_=true;
        // TODO: This could change for a timer in the updateAction method,
        // to just change the image when x time has elapsed.
        boost::this_thread::sleep(boost::posix_time::seconds(iImageTime_));
        //std::string str = attention_ ? "attention" : "noAtt";
        //std::cout << str << std::endl;

    }
}

void AttentionActioner::setImageDatabase(ImageDatabase *pNewImageDatabase)
{
    pImageDatabase_ = pNewImageDatabase;
}

void AttentionActioner::setImageTime(int iNewImageTime_)
{
    iImageTime_ = iNewImageTime_;
}

void AttentionActioner::logAttentionRatio()
{
    if(dataEntry < maxDataEntries) {
        file_ << dataEntry++ << " " << (bDrawingAttention_ == attention_) << std::endl;
    } else { //Force user to press any key to finish the experiment.
        decoyImg_.setTo(cv::Scalar(255, 255, 255));
        image1.setTo(cv::Scalar(255, 255, 255));
        imageLogger.drawTitle("Experiment finished!!", image1);
        imageLogger.drawText("Press any key to exit.", image1);
        imageLogger.drawTitle("Experiment finished!!", decoyImg_);
        imageLogger.drawText("Press any key to exit.", decoyImg_);
    }
}

void AttentionActioner::setResultsFilename(std::string newResultsFilename)
{
    resultsFilename = newResultsFilename;
    file_.open(resultsFilename.c_str(), std::ios::out | std::ios::app);

    std::string name;
    std::cout << "Please write down your name: ";
    std::cin >> name;
    std::cout << "your name was: " << name << std::endl;
    int subID;
    std::cout << "Please write down your subject (numerical) ID: ";
    std::cin >> subID;
    std::cout << "Your subject ID was: " << subID << std::endl;
    file_ << "% Subject " << subID << " (" << name << ")" << std::endl;
}

void AttentionActioner::setMaxDataEntries(long long newMaxDataEntries)
{
    maxDataEntries = newMaxDataEntries;
}

} // namespace lcg_face_attention

