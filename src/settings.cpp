// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#include "settings.h"

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace lcg_face_attention {

Settings::Settings()
{
}

void Settings::log()
{
    // Useful using directives
    using std::cout;
    using std::endl;

    cout << "Logging settings of the current configuration file: "
         << configFilename_ << endl;
    std::map<std::string, std::string>::iterator mapIt;
    for(mapIt = xmlNodeValues_.begin(); mapIt != xmlNodeValues_.end(); mapIt++) {
        cout << mapIt->first << " => " << mapIt->second << endl;
    }
}

void Settings::populateAllSubnodes(boost::property_tree::ptree xmlTree,
                                   std::string xmlTreeNode,
                                   std::string nodeQualifiedName)
{
    BOOST_FOREACH(const boost::property_tree::ptree::value_type &node,
                  xmlTree.get_child(xmlTreeNode)) {
        // node.first is the name of the child.
        // node.second is the child tree.
        std::stringstream tempSs;
        boost::property_tree::ptree subtree = (boost::property_tree::ptree) node.second;
        if(!subtree.empty()) { // There are more nodes on the path
            tempSs << nodeQualifiedName << node.first.data() << "."; // Prepare node path
            populateAllSubnodes(subtree, "", tempSs.str()); // Resolve subtree
        } else { // Deepest node on the path
            tempSs << nodeQualifiedName << node.first.data(); // Prepare last node path
            xmlNodeValues_[tempSs.str()] = node.second.data();

            #ifndef NDEBUG      //-----------Debugging code
            // Log node name and content
            std::cout << tempSs.str() << " => "
                      << xmlNodeValues_[tempSs.str()] << std::endl;
            #endif              //-----------End of debugging code
        }
    }
}

void Settings::readSettings(const std::string &filename)
{
    configFilename_ = filename;

    // Dump settings xml file into a property tree
    boost::property_tree::ptree pt;
    read_xml(configFilename_, pt);

    // Pupulate xmlNodeValues_
    populateAllSubnodes(pt);

    // If compiled on windows environment
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    // Replace / (*nix paths) to \ (windows paths)
    std::map<std::string, std::string>::iterator mapIt;
    for(mapIt = xmlNodeValues_.begin(); mapIt != xmlNodeValues_.end(); mapIt++) {
        std::replace(mapIt->second.begin(), mapIt->second.end(), '/', '\\');

        #ifndef NDEBUG      //-----------Debugging code
        // Log paths
        std::cout << "Windows path: " << mapIt->first << " => "
                  << mapIt->second << std::endl;
        #endif              //-----------End of debugging code
    }
#endif
}

} // namespace lcg_face_attention
