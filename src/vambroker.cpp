// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#include "vambroker.h"

#include <iostream>
#include <opencv2/highgui/highgui.hpp>

#include "eyeattentiondetector.h"

namespace lcg_face_attention {

VAMBroker::VAMBroker(AttentionInterface* detector) :
    baseVAMBroker(detector)
{
}

VAMBroker::~VAMBroker()
{
    delete pActioner_;
    delete pImageLogger_;
}

void VAMBroker::onImageAcquired(cv::Mat &image)
{
    pImage_ = &image;
    attentionDetector->detect(image);
    //cv::imshow( "Frontal and profile face detector", image);
}

void VAMBroker::onAttentionChanged(AttentionEvent *event)
{
    switch(event->getOrigin()){
    case AttentionEvent::FACE_NOT_DETECTED :
        if(attentionRules_["face"]){ // If the face needs to be detected in order to gather attention
            attentionLost();
        } else { // If not detecting a face means gathering attention
            attentionCaptured();
        }
        std::cout << "Face not detected " << std::endl;
        break;

    case AttentionEvent::EYE_NOT_DETECTED :
        if(attentionRules_["eyes"]){ // If the eyes needs to be detected in order to gather attention
            attentionLost();
        } else { // If not detecting the eyes means gathering attention
            attentionCaptured();
        }
        std::cout << "Eye not detected " << std::endl;
        break;

    case AttentionEvent::IRIS_NOT_FOUND :
        if(attentionRules_["iris"]){ // If the iris needs to be detected in order to gather attention
            attentionLost();
        } else { // If not detecting the iris means gathering attention
            attentionCaptured();
        }
        std::cout << "Eye not detected " << std::endl;
        break;


    case AttentionEvent::EYE_NOT_IN_CENTER :
        if(attentionRules_["iris_center"]){ // If the iris needs to be on the center of the eye in order to gather attention
            attentionLost();
        } else { // If not placing the iris in the center of the eye means gathering attention
            attentionCaptured();
        }
        std::cout << "Eye not in center " << std::endl;
        break;

    case AttentionEvent::EYE_IN_CENTER :
        if(attentionRules_["iris_center"]){ // The iris is on the center, thus full attention
            attentionCaptured();
        } else {
            attentionLost();
        }
        std::cout << "Eye attention " << std::endl;
        break;
    }

//    // If the face and eye is not detected, the user is loosing attention
//    if(event->getOrigin() == AttentionEvent::FACE_NOT_DETECTED
//            || event->getOrigin() == AttentionEvent::EYE_NOT_DETECTED) {
//        attentionLost();
//    } else {
//        attentionCaptured();
//    }

//    std::cout << "Attention: " << event->getAttention() << std::endl;
}

void VAMBroker::attentionLost()
{
    std::cout << "Attention lost!!!!!!!!!!!" << std::endl;
    pImageLogger_->drawText("Attention lost", *pImage_);
    actionEvent_.setRunning(false);
    pActioner_->updateAction(actionEvent_);
}

void VAMBroker::attentionCaptured()
{
    std::cout << "Attention captured!!!!!!!!" << std::endl;
    pImageLogger_->drawText("Attention captured", *pImage_);
    actionEvent_.setRunning(true);
    pActioner_->updateAction(actionEvent_);
}

void VAMBroker::setImageLogger(ImageLogger *pNewImageLogger)
{
    pImageLogger_ = pNewImageLogger;
}

void VAMBroker::setActioner(ActionerInterface *pNewActioner)
{
    pActioner_ = pNewActioner;
}

void VAMBroker::setAttentionRules(std::map<std::string, bool> newAttentionRules)
{
    attentionRules_ = newAttentionRules;
}

} // namespace lcg_face_attention
