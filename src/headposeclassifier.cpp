// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#include "headposeclassifier.h"

#include <iostream>

namespace lcg_face_attention {

HeadPoseClassifier::HeadPoseClassifier()
    : scale_(1)
{
}

// TODO - Refactor this
std::vector<cv::Rect> HeadPoseClassifier::detect(cv::Mat &rOriginalImage)
{
    //---------> Preprocess original image
    int imageWidth = cvRound(rOriginalImage.cols/scale_);
    cv::Mat downsampledImage( cvRound (rOriginalImage.rows/scale_), imageWidth, CV_8UC1 );
    cv::Mat grayImage;
    cv::cvtColor( rOriginalImage, grayImage, CV_BGR2GRAY ); // Turn image into grayscale
    // Downsample image
    cv::resize( grayImage, downsampledImage, downsampledImage.size(), 0, 0, cv::INTER_LINEAR );
    cv::equalizeHist( downsampledImage, downsampledImage ); // Increase image contrast

    #ifndef NDEBUG      //-----------Debugging code
    using std::cout;
    using std::endl;
    cout << "Input image size: [" << rOriginalImage.rows << ", "
         << rOriginalImage.cols << "]" << endl;

    cout << "Downsampled image size: [" << downsampledImage.rows << ", "
         << downsampledImage.cols << "]" << endl;
    #endif              //-----------End of debugging code

    //---------> Log useful information to image
    std::stringstream tmpStr;
    tmpStr << "Original image resolution: [" << rOriginalImage.rows << ", "
           << rOriginalImage.cols << "]";
    imageLogger_->drawText( tmpStr.str(), rOriginalImage );

    tmpStr.str(std::string()); // Clear stream
    tmpStr << "Reduced image resolution: [" << downsampledImage.rows << ", "
           << downsampledImage.cols << "]";
    imageLogger_->drawText(tmpStr.str(), rOriginalImage);

    tmpStr.str(std::string()); // Clear stream
    tmpStr << "Scale used 1/" << scale_;
    imageLogger_->drawText(tmpStr.str(), rOriginalImage);

    imageLogger_->drawText(" ", rOriginalImage);
    imageLogger_->drawText("Detection times: ",rOriginalImage);
    tmpStr.precision(2);

    std::vector<cv::Rect> facesList;
    unsigned short int poseCounter = 0; // TODO - define this variable on the while loop
    // Variables which scope is inside the loop, but defined here for eficiency
    double time = 0;
    EnhancedCascadeClassifier tmpFacePoseClassifier;
    std::queue<EnhancedCascadeClassifier> tmpClassifiers = headPosesClassifiers_;
    while(!tmpClassifiers.empty()) {
        tmpFacePoseClassifier = tmpClassifiers.front();
        cv::Mat disposableImage = downsampledImage.clone();

        time = static_cast<double>(cv::getTickCount());
        // Apply specific classifier image transformations
        applyClassifierTransformations(disposableImage, tmpFacePoseClassifier);

        tmpFacePoseClassifier.detectMultiScale( disposableImage, facesList,
                                          1.1, 2, 0
                                          |CV_HAAR_FIND_BIGGEST_OBJECT
                                          //|CV_HAAR_DO_ROUGH_SEARCH
                                          |CV_HAAR_SCALE_IMAGE
                                          ,
                                          cv::Size(30, 30) );

        time = static_cast<double>(cv::getTickCount()) - time;

        // Log detection time to image
        tmpStr.str(std::string()); // Clear stream
        tmpStr<<std::fixed;
        tmpStr << tmpFacePoseClassifier.getName() << " " << ++poseCounter << " = "
               << time/static_cast<double>(cv::getTickFrequency())*1000.0 << "ms";
        imageLogger_->drawText(tmpStr.str(),rOriginalImage);

        #ifndef NDEBUG      //-----------Debugging code
        cout << "Detection time, " << tmpStr.str() << endl;
        #endif              //-----------End of debugging code

        if(!facesList.empty()) { //If a face pose was found
            imageLogger_->drawTitle(tmpFacePoseClassifier.getName(), rOriginalImage);
            rectTransformations_.absoluteScale(facesList, scale_);
            break;
            //TODO - this should be expanded to support multiple poses detected
        }

        // Push actual face pose to the back, so in the next search,
        // it would be searched at the end
        tmpClassifiers.pop();
        headPosesClassifiers_.pop();
        headPosesClassifiers_.push(tmpFacePoseClassifier);
    }

    return facesList;
}

cv::Mat HeadPoseClassifier::rotateImage(const cv::Mat& rSource, double angle) const
{
    cv::Point2f src_center(rSource.cols/2.0F, rSource.rows/2.0F);
    cv::Mat rot_mat = cv::getRotationMatrix2D(src_center, angle, 1.0);
    cv::Mat dst;
    cv::warpAffine(rSource, dst, rot_mat, rSource.size());
    return dst;
}

void HeadPoseClassifier::applyClassifierTransformations(cv::Mat &rImage,
                                                        const EnhancedCascadeClassifier& rFaceClassifier) const
{
    // Image mirroring and rotation
    if(rFaceClassifier.isMirror()) {
        cv::flip(rImage, rImage, 1); // Mirror image
    }
    if(rFaceClassifier.getRotation() != 0.0) {
        rImage = rotateImage(rImage,
                             rFaceClassifier.getRotation());
    }
}

} // namespace lcg_face_attention
