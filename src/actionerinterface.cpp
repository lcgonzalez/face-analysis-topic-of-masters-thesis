#include "actionerinterface.h"

namespace lcg_face_attention {

ActionerInterface::ActionerInterface()
{
    m_Thread = boost::thread(&ActionerInterface::action, this);
}

void ActionerInterface::join()
{
    m_Thread.join();
}

ActionerInterface::~ActionerInterface()
{
}

} // namespace lcg_face_attention
