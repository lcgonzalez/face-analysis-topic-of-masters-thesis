#include "actionevent.h"

namespace lcg_face_attention {

ActionEvent::ActionEvent()
{
}

void ActionEvent::setRunning(bool newRunning)
{
    running = newRunning;
}

bool ActionEvent::isRunning()
{
    return running;
}

} // namespace lcg_face_attention
