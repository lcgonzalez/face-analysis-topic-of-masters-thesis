#!/bin/sh
echo "Script designed to run all the tests of the face-attention-detection program"
echo "Please write down your first name, followed by [ENTER]: "
read name
echo "Please write down your user ID (digit), followed by [ENTER]: "
read ID

echo "$name $ID" | ../build/face-attention-detection --config-file=./settings1.config
echo "$name $ID" | ../build/face-attention-detection --config-file=./settings2.config
echo "$name $ID" | ../build/face-attention-detection --config-file=./settings3.config
