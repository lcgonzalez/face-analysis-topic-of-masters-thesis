/*/////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//                          License Agreement
//               For the Face Analysis and Attention Detector
//
// Copyright (c) 2013, Luis Carlos González García, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   1. Redistributions of source code must retain the above copyright notice,
//      this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright notice,
//      this list of conditions and the following disclaimer in the documentation
//      and/or other materials provided with the distribution.
//
//   3. Neither the name of the CINVESTAV, nor the names of its contributors may
//      be used to endorse or promote products derived from this software without
//      specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*/

#include <opencv2/objdetect/objdetect.hpp> /*CascadeClassifier*/
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <stdio.h>


#include "headposeclassifier.h"
#include "enhancedcascadeclassifier.h"
#include "eyeattention.h"
#include "vambroker.h"

using namespace std;
using namespace cv;
//CV_MAJOR_VERSION
//CV_MINOR_VERSION



void help()
{
    cout << "\nThis program demonstrates the cascade recognizer. Now you can use Haar or LBP features.\n"
            "This classifier can recognize many ~rigid objects, it's most known use is for faces.\n"
            "Usage:\n"
            "./facedetect [--face-cascade=<face_cascade_path> this is the primary frontal face trained classifier]\n"
            "   [--nested-cascade[=nested_cascade_path this an optional secondary classifier such as eyes]]\n"
            "   [--scale=<image scale greater or equal to 1, try 1.3 for example>\n"
            "   [filename|camera_index]\n\n"
            "see facedetect.cmd for one call:\n"
            "./facedetect --cascade=\"../../data/haarcascades/haarcascade_frontalface_alt.xml\" --nested-cascade=\"../../data/haarcascades/haarcascade_eye.xml\" --scale=1.3 \n"
            "Hit any key to quit.\n"
            "Using OpenCV version  " << CV_VERSION << "\n" << endl;
}

void detectAndDraw(Mat& img, HeadPoseClassifier& headClassifier,
                   CascadeClassifier& rightEyeCascade, CascadeClassifier& leftEyeCascade,
                   CascadeClassifier& mouthCascade, double scale, Mat& croppedFace);

String frontalFaceCascadeName = ":/haarcascade/data/haarcascades/haarcascade_frontalface_alt2.xml";
String profileFaceCascadeName = "../opencv_facedetect_sample/data/haarcascades/haarcascade_profileface.xml";
String rightEyeCascadeName = "../opencv_facedetect_sample/data/haarcascades/haarcascade_righteye_2splits.xml";
String leftEyeCascadeName = "../opencv_facedetect_sample/data/haarcascades/haarcascade_lefteye_2splits.xml";
String mouthCascadeName = "../opencv_facedetect_sample/data/haarcascades/haarcascade_mcs_mouth.xml";

int main( int argc, const char** argv )
{
    VideoCapture capture;
    Mat frame, frameCopy, image, croppedFrame;
    const String scaleOpt = "--scale=";
    size_t scaleOptLen = scaleOpt.length();
    const String faceCascadeOpt = "--face-cascade=";
    size_t faceCascadeOptLen = faceCascadeOpt.length();
    const String profileFaceCascadeOpt = "--profile-face-cascade=";
    size_t profileFaceCascadeOptLen = profileFaceCascadeOpt.length();
    const String rightEyeCascadeOpt = "--right-eye-cascade";
    size_t rightEyeCascadeOptLen = rightEyeCascadeOpt.length();
    const String leftEyeCascadeOpt = "--left-eye-cascade";
    size_t leftEyeCascadeOptLen = leftEyeCascadeOpt.length();
    const String mouthCascadeOpt = "--mouth-cascade";
    size_t mouthCascadeOptLen = mouthCascadeOpt.length();
    String inputName;

    help();

    //QFileInfo info1;
    //info1.setFile("qrc:///haarcascade/data/haarcascades/haarcascade_frontalface_alt2.xml");
    //cout << info1.absoluteFilePath().toStdString() << endl;
    //frontalFaceCascadeName.assign("qrc:///haarcascade/data/haarcascades/haarcascade_frontalface_alt2.xml");

    EnhancedCascadeClassifier faceCascade, leftProfileFaceCascade, rightProfileFaceCascade, rightEyeCascade, leftEyeCascade, mouthCascade;
    double scale = 1;

    for( int i = 1; i < argc; i++ )  //Arguments options selection and storing
    {
        cout << "Processing " << i << " " <<  argv[i] << endl;
        if( faceCascadeOpt.compare( 0, faceCascadeOptLen, argv[i], faceCascadeOptLen ) == 0 )
        {
            frontalFaceCascadeName.assign( argv[i] + faceCascadeOptLen );
            cout << "  from which we have frontalFaceCascadeName= " << frontalFaceCascadeName << endl;
            if( !faceCascade.load( frontalFaceCascadeName ) )
                cerr << "WARNING: Could not load classifier cascade for frontal face" << endl;
        }
        //        else {
        //            cout << "  Loading default  frontalFaceCascadeName= " << frontalFaceCascadeName << endl;
        //            if( !faceCascade.load( frontalFaceCascadeName ) )
        //                cerr << "WARNING: Could not load classifier cascade for frontal face" << endl;
        //        }
        else if( profileFaceCascadeOpt.compare( 0, profileFaceCascadeOptLen, argv[i], profileFaceCascadeOptLen ) == 0 )
        {
            profileFaceCascadeName.assign( argv[i] + profileFaceCascadeOptLen );
            cout << "  from which we have frontalFaceCascadeName= " << profileFaceCascadeName << endl;
            if( !leftProfileFaceCascade.load( profileFaceCascadeName ) )
                cerr << "WARNING: Could not load classifier cascade for profile face" << endl;
        }
        else if( rightEyeCascadeOpt.compare( 0, rightEyeCascadeOptLen, argv[i], rightEyeCascadeOptLen ) == 0 )
        {
            if( argv[i][rightEyeCascadeOpt.length()] == '=' )
                rightEyeCascadeName.assign( argv[i] + rightEyeCascadeOpt.length() + 1 );
            if( !rightEyeCascade.load( rightEyeCascadeName ) )
                cerr << "WARNING: Could not load classifier cascade for right eye" << endl;
        }
        else if( leftEyeCascadeOpt.compare( 0, leftEyeCascadeOptLen, argv[i], leftEyeCascadeOptLen ) == 0 )
        {
            if( argv[i][leftEyeCascadeOpt.length()] == '=' )
                leftEyeCascadeName.assign( argv[i] + leftEyeCascadeOpt.length() + 1 );
            if( !leftEyeCascade.load( leftEyeCascadeName ) )
                cerr << "WARNING: Could not load classifier cascade for left eye" << endl;
        }
        else if(mouthCascadeOpt.compare(0, mouthCascadeOptLen, argv[i], mouthCascadeOptLen) == 0) {
            if( argv[i][mouthCascadeOpt.length()] == '=' )
                mouthCascadeName.assign( argv[i] + mouthCascadeOpt.length() + 1 );
            if( !mouthCascade.load( mouthCascadeName ) )
                cerr << "WARNING: Could not load classifier cascade for mouth" << endl;
        }
        else if( scaleOpt.compare( 0, scaleOptLen, argv[i], scaleOptLen ) == 0 )
        {
            if( !sscanf( argv[i] + scaleOpt.length(), "%lf", &scale ) || scale < 1 )
                scale = 1;
            cout << " from which we read scale = " << scale << endl;
        }
        else if( argv[i][0] == '-' )
        {
            cerr << "WARNING: Unknown option %s" << argv[i] << endl;
        }
        else
            inputName.assign( argv[i] );
    }

    if( !faceCascade.load( frontalFaceCascadeName ) )
    {
        cerr << "ERROR: Could not load classifier cascade" << endl;
        cerr << "Usage: facedetect [--cascade=<cascade_path>]\n"
                "   [--nested-cascade[=nested_cascade_path]]\n"
                "   [--scale[=<image scale>\n"
                "   [filename|camera_index]\n" << endl ;
        return -1;
    }



    if( inputName.empty() || (isdigit(inputName.c_str()[0]) && inputName.c_str()[1] == '\0') ) //If the video source is a camera
    {
        capture.open(inputName.empty() ? 0 : inputName.c_str()[0] - '0');
        int c = inputName.empty() ? 0 : inputName.c_str()[0] - '0';
        if(!capture.isOpened()) cout << "Capture from CAM " <<  c << " didn't work" << endl;
    }
    else if( inputName.size() ) //If the video source is a file
    {
        image = imread( inputName, 1 );
        if( image.empty() )
        {
            capture.open(inputName.c_str());
            if(!capture.isOpened()) cout << "Capture from AVI didn't work" << endl;
        }
    }
    else
    {
        image = imread( "lena.jpg", 1 );
        if(image.empty()) cout << "Couldn't read lena.jpg" << endl;
    }


    EnhancedCascadeClassifier rotatedLeftFrontalFaceCascade, rotatedRightFrontalFaceCascade;

    rotatedLeftFrontalFaceCascade = faceCascade;
    rotatedLeftFrontalFaceCascade.setRotation(45);
    rotatedRightFrontalFaceCascade = faceCascade;
    rotatedRightFrontalFaceCascade.setRotation(-45);

    rightProfileFaceCascade = leftProfileFaceCascade;
    rightProfileFaceCascade.setMirror(true);

    faceCascade.setName("Frontal face");
    leftProfileFaceCascade.setName("Left profile");
    rightProfileFaceCascade.setName("Right profile");
    rotatedLeftFrontalFaceCascade.setName("45° rotation");
    rotatedRightFrontalFaceCascade.setName("-45° rotation");

    HeadPoseClassifier headClassifier;
    headClassifier.addClassifier(faceCascade);
    headClassifier.addClassifier(leftProfileFaceCascade);
    headClassifier.addClassifier(rightProfileFaceCascade);
    //headClassifier.addClassifier(rotatedLeftFrontalFaceCascade);
    //headClassifier.addClassifier(rotatedRightFrontalFaceCascade);
    headClassifier.setScale(scale);

    vector<Rect> faces;
    EyeAttention* eyeAttention = new EyeAttention();
    eyeAttention->setClassifier(leftEyeCascade);
    eyeAttention->setHeadDetector(headClassifier);

    VAMBroker broker(eyeAttention); //Connect callback

    if( capture.isOpened() )
    {
        cout << "In capture ..." << endl;

        for(;;)
        {
            capture >> frame;
            if( frame.empty() )
                break;

            frame.copyTo( frameCopy );

            //detectAndDraw( frameCopy, headClassifier, rightEyeCascade, leftEyeCascade, mouthCascade, scale, croppedFrame );

            //faces = headClassifier.detect(frameCopy);
            //eyeAttention->detectAttention(frameCopy, faces);
            broker.onImageAcquired(frameCopy);

            if( waitKey( 10 ) >= 0 ) //cleanup
                capture.release();
        }
        waitKey(0);
    }
    else
    {
        cout << "In image read" << endl;
        if( !image.empty() )
        {
            //detectAndDraw( image, headClassifier, rightEyeCascade, leftEyeCascade,mouthCascade, scale, croppedFrame );
            broker.onImageAcquired(image);
            waitKey(0);
        }
        else if( !inputName.empty() )
        {
            /* assume it is a text file containing the
            list of the image filenames to be processed - one per line */
            FILE* f = fopen( inputName.c_str(), "rt" );
            if( f )
            {
                char buf[1000+1];
                while( fgets( buf, 1000, f ) )
                {
                    int len = (int)strlen(buf), c;
                    while( len > 0 && isspace(buf[len-1]) )
                        len--;
                    buf[len] = '\0';
                    cout << "file " << buf << endl;
                    image = imread( buf, 1 );
                    if( !image.empty() )
                    {
                        detectAndDraw( image, headClassifier, rightEyeCascade, leftEyeCascade, mouthCascade, scale, croppedFrame );
                        c = waitKey(0);
                        if( c == 27 || c == 'q' || c == 'Q' )
                            break;
                    }
                    else
                    {
                        cerr << "Aw snap, couldn't read image " << buf << endl;
                    }
                }
                fclose(f);
            }
        }
    }

    cvDestroyWindow("result");

    return 0;
}

void detectAndDraw(Mat& img, HeadPoseClassifier& headClassifier,
                   CascadeClassifier& rightEyeCascade, CascadeClassifier &leftEyeCascade,
                   CascadeClassifier &mouthCascade, double scale, Mat& croppedFace)
{
    double t = 0;
    vector<Rect> faces;
    vector<Rect> nestedMouth;
    const static Scalar colors[] =  { CV_RGB(0,0,255), //Colors for each object detected
                                      CV_RGB(0,128,255),
                                      CV_RGB(0,255,255),
                                      CV_RGB(0,255,0),
                                      CV_RGB(255,128,0),
                                      CV_RGB(255,255,0),
                                      CV_RGB(255,0,0),
                                      CV_RGB(255,0,255)} ;
    //    int imageWidth = cvRound(img.cols/scale);
    //    Mat gray, smallImg( cvRound (img.rows/scale), imageWidth, CV_8UC1 );
    //    printf("%d %d \n", img.rows, img.cols);

    faces = headClassifier.detect(img);
    EyeAttention* eyeAttention = new EyeAttention();
    VAMBroker broker(eyeAttention);

    eyeAttention->setClassifier(leftEyeCascade);
    eyeAttention->setHeadDetector(headClassifier);
    eyeAttention->detectAttention(img, faces);


    //    Rect mouthAreaSearch;

    //        if( mouthCascade.empty() )
    //            continue;
    //        //use original resolution image
    //        mouthAreaSearch = rectangleObj;
    //        mouthAreaSearch.height /= 2;
    //        mouthAreaSearch.y += mouthAreaSearch.height;
    //        smallImgROI = gray(mouthAreaSearch).clone();
    //        equalizeHist(smallImgROI, smallImgROI);

    //        t = (double)cvGetTickCount();

    //        mouthCascade.detectMultiScale( smallImgROI, nestedMouth,
    //            1.1, 2, 0
    //            |CV_HAAR_FIND_BIGGEST_OBJECT
    //            |CV_HAAR_DO_ROUGH_SEARCH
    //            //|CV_HAAR_DO_CANNY_PRUNING
    //            //|CV_HAAR_SCALE_IMAGE
    //            ,
    //            Size(20, 20) );
    //        t = (double)cvGetTickCount() - t;
    //        printf( "detection time, mouth = %g ms\n", t/((double)cvGetTickFrequency()*1000.) );
    //        tmpStr.str(string());
    //        tmpStr << "Mouth: "<< t/((double)cvGetTickFrequency()*1000) << "ms";
    //        headClassifier.drawDetectionTime(tmpStr.str(),img);

    //        for( vector<Rect>::const_iterator nm = nestedMouth.begin(); nm != nestedMouth.end(); nm++ )
    //        {
    //            color=colors[4];
    //            center.x = cvRound(((r->x)*scale + nm->x + nm->width*0.5));
    //            center.y = cvRound(((r->y)*scale + nm->y + mouthAreaSearch.height + nm->height*0.5));
    //            radius = cvRound((nm->width + nm->height)*0.25);
    //            circle( img, center, radius, color, 3, 8, 0 );
    //        }

    //    //Sobel
    //    Mat grad_x;
    //    Mat grad_y;
    //    Mat absGradX;
    //    Mat absGradY;
    //    Mat grad;

    //    //HoughLines
    //    Mat dstHough;
    //    vector<Vec4i> lines;
}

//../opencv-face-and-eye-detect/data/videos_gestos/luis_interaccion_sin_lentes.webm
