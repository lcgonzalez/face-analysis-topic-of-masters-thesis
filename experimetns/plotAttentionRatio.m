% Plot the attention ratio results from all subjects
load ../build/resultsNotIris.data
y = resultsNotIris(:,2);
for i=1:length(y);
    x(i) = i;
end
figure('name', 'Attention accuracy graph of the Eyes approach');
plot(x,y)
ylim([-0.1 1.3]);
avg = mean(y);
line(xlim(), [avg avg], 'Color', [0,1,0], 'LineStyle', '--');
title('Attention accuracy graph of the Eyes approach');
legend('Attention accuracy','Mean attention accuracy');
xlabel('Number of data probes') % x-axis label
ylabel('Attention accuracy') % y-axis label


load ../build/resultsIris.data
y = resultsIris(:,2);
x=0;
for i=1:length(y);
    x(i) = i;
end
figure('name', 'Attention accuracy graph of the Iris approach');
plot(x,y)
ylim([-0.1 1.3]);
avg = mean(y);
line(xlim(), [avg avg], 'Color', [0,1,0], 'LineStyle', '--');
title('Attention accuracy graph of the Iris approach');
legend('Attention accuracy','Mean attention accuracy');
xlabel('Number of data probes') % x-axis label
ylabel('Attention accuracy') % y-axis label


load ../build/resultsIrisCenter.data
y = resultsIrisCenter(:,2);
x=0;
for i=1:length(y);
    x(i) = i;
end
figure('name', 'Attention accuracy graph of the Iris Position approach');
plot(x,y)
ylim([-0.1 1.3]);
avg = mean(y);
line(xlim(), [avg avg], 'Color', [0,1,0], 'LineStyle', '--');
title('Attention accuracy graph of the Iris Position approach');
legend('Attention accuracy','Mean attention accuracy');
xlabel('Number of data probes') % x-axis label
ylabel('Attention accuracy') % y-axis label