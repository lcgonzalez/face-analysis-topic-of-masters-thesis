IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.

By downloading, copying, installing or using the software you agree to this license.
If you do not agree to this license, do not download, install,
copy or use the software.

                          License Agreement
               For the Face Analysis and Attention Detector

Copyright (c) 2013, Luis Carlos González García, all rights reserved.
Third party copyrights are property of their respective owners.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the CINVESTAV, nor the names of its contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Author: Luis Carlos González García

Instructions to correctly build and run the face attention detector program "face-attention-detection" written to support the Master's thesis of Luis Carlos González García.

Dependencies: OpenCV 2.4.3 and Boost 1.46.1
*
It is assumed that the build tool is "*CMake*".

Instructions for Linux:
This file was written in "markdown", so to correctly display it run the following command: 
  markdown README.md > README.html

Building:
Create a directory to build the project, and build the project inside it:

  * $ mkdir build
  * $ cd build
  * $ cmake ../
  * $ make

Build with unit tests and run the tests (optional):

  * $ cmake -D BUILD_TESTING=yes ../
  * $ make
  * $ make test

Build with debug symbols (optional):

  * $ cmake -D CMAKE\_BUILD\_TYPE=Debug ../
  * $ make


To correctly run the program on a test environment, please download
the dataset videos from the designated url's to your computer.

Download the videos from
**Dropbox** :

  * [https://www.dropbox.com/s/3zrid4r1uqbjc3h/kinect\_front\_noAttention.avi](https://www.dropbox.com/s/3zrid4r1uqbjc3h/kinect_front_noAttention.avi)
  * [https://www.dropbox.com/s/umwbarglv7lpvt3/kinect\_front\_attention.avi](https://www.dropbox.com/s/umwbarglv7lpvt3/kinect_front_attention.avi)

Then run the program providing the configuration file as follows: 
  face-attention-detection --config-file=../settings.config
